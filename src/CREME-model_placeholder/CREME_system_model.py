# -*- coding: utf-8 -*-
import openmdao.api as om
from CREME_MDA import *


# XDSM generation
from omxdsm import write_xdsm

prob_CREME = om.Problem()
prob_CREME.model = CREME_MDA()

prob_CREME.model.add_design_var('OrbitDynamics.a')
#prob_CREME.model.add_design_var('OrbitDynamics.exc')
#prob_CREME.model.add_design_var('OrbitDynamics.inc')
#prob_CREME.model.add_design_var('OrbitDynamics.raan')
#prob_CREME.model.add_design_var('OrbitDynamics.aop')
#prob_CREME.model.add_design_var('OrbitDynamics.ta')
prob_CREME.model.add_design_var('Power.batteryCap')
prob_CREME.model.add_design_var('Power.panelsPower')
prob_CREME.model.add_design_var('Power.dischargePower')
prob_CREME.model.add_design_var('Data.bandwidthUplink')
prob_CREME.model.add_design_var('Data.bandwidthDownlink')
#constraints
prob_CREME.model.add_constraint('Power.charge', lower=0.7) #30% discharge
prob_CREME.model.add_constraint('Link.marginUpload', lower=5) #dB
# objectives
prob_CREME.model.add_objective('Data.downlinkTotal')

prob_CREME.setup()
prob_CREME.run_model()
resp = prob_CREME.get_val('Data.downlinkTotal')
print(resp)

# OpenMDAO-XDSM test, added by user
# Write output. PDF will only be created, if pdflatex is installed
# to be resolved - output of designVars* from each element of the group - that shouldn't be happening
write_xdsm(prob_CREME, filename='CREME_XDSM_pyxdsm', out_format='html', show_browser=True,
            quiet=False, output_side='right', include_indepvarcomps=False)
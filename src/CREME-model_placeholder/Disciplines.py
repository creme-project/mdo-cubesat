# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

All the vars here are just a placeholders to generate a XDSM
The actual model will be based of this rudimentary version. Analytical expressions will be aplied whenever possible
"""

import openmdao.api as om

class OrbitalDynamics(om.ExplicitComponent):
    """
    Orbital Dynalics discipline
    """
    def setup(self):
        #input variables
        self.add_input('a', val=7000.0) #SMA [km]
        self.add_input('exc', val=0.0) # excentricity
        self.add_input('inc', val=90.0) # inclination [deg]
        self.add_input('raan', val=270) # raan [deg]
        self.add_input('aop', val=0) # argument of periapsis [deg]
        self.add_input('ta', val=0) # true anomaly [deg]
        #output variables
        self.add_output('eclipses', val=[0])
        self.add_output('gsVisibility', val=[0])
        self.add_output('altitudeMax', val=0) 
           
    
    def compute(self, inputs, outputs):
        a = inputs['a']
        exc = inputs['exc']
        
        outputs['eclipses'] = 0
        outputs['gsVisibility'] = 0
        outputs['altitudeMax'] = a*(1 + exc) - 6378 # [km]
    
    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')
      
#----------------------------------------------------------------------
class Power(om.ExplicitComponent):
    """
    Power management discipline
    """
    
    def setup(self):
        #input variables
        self.add_input('eclipses', val=[0])
        self.add_input('gsVisibility', val=[0])
        self.add_input('batteryCap', val=0)
        self.add_input('panelsPower', val=0)
        self.add_input('dischargePower', val=[0])
        
        #output variables
        self.add_output('charge', val=[0])
        
    def compute(self, inputs, outputs):
        outputs['charge'] = [0]
    
    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

#----------------------------------------------------------------------        
class Data(om.ExplicitComponent):
    """
    Data transmission discipline
    """
    
    def setup(self):
        #input variables
        self.add_input('gsVisibility', val=[0])
        self.add_input('bandwidthUplink', val=0)
        self.add_input('bandwidthDownlink', val=0)
        
        #output variables
        self.add_output('uplink', val=[0])
        self.add_output('downlink', val=[0])
        self.add_output('uplinkTotal', val=0)
        self.add_output('downlinkTotal', val=0)
    
    def compute(self, inputs, outputs):
        outputs['uplink'] = [0] # Mbit (more for information)
        outputs['downlink'] = [0] # Mbit
        outputs['uplinkTotal'] = 0 # Mbit
        outputs['downlinkTotal'] = 0 # Mbit
        
        
    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')
        
#----------------------------------------------------------------------
class Link(om.ExplicitComponent):
    """
    Link budget verification
    """
    
    def setup(self):
        #input variables
        self.add_input('altitudeMax', val=0)
        
        #output variables
        self.add_output('marginUpload', val=0) # [dB]
        self.add_output('marginDownload', val=0) # [dB]
    
    def compute(self, inputs, outputs):
        outputs['marginUpload']=0
        outputs['marginDownload']=0
    
    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')
        
        
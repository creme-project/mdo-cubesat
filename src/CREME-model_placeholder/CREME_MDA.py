# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

MDA part of the CREME optimization problem
"""

import openmdao.api as om
from Disciplines import *

class CREME_MDA(om.Group):
    """
    MDA part of the CREME optimization problem as an instance of openmdao.api.Group
    """
    
    def setup(self):
        self.add_subsystem('OrbitDynamics', OrbitalDynamics())
        self.add_subsystem('Power', Power())
        self.add_subsystem('Data', Data())
        self.add_subsystem('Link', Link())
        
        #internal connections
        self.connect('OrbitDynamics.eclipses', 'Power.eclipses')
        self.connect('OrbitDynamics.gsVisibility', 'Power.gsVisibility')
        self.connect('OrbitDynamics.gsVisibility', 'Data.gsVisibility')
        self.connect('OrbitDynamics.altitudeMax', 'Link.altitudeMax')
        
        #self.nonlinear_solver = om.NewtonSolver(solve_subsystems=False)
        #self.linear_solver = om.DirectSolver()
        #external connections

# --------------debugging-------------------

if __name__ == "__main__":
    prob_CREME = om.Problem()
    prob_CREME.model = CREME_MDA()

    prob_CREME.setup()

    prob_CREME.run_model()
    resp = prob_CREME.get_val('Data.downlinkTotal')
    print(resp)
# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

MDAO ipmlementation of the CREME advanced optimization problem

includes 2 classes; to better distinguish between specific requirements of SciPyDriver and DifferentialEvolutionDriver.
Signifficant difference - scaling of objectives, constraints and variables in order to force DE driver to respect the penalisation for violating constraints.
"""
from matplotlib import pyplot as plt
import openmdao.api as om
from DisciplinesAnalyticPartials import *
from functionsDatabase import getBERMargins, plotEvolutions, plotConvergence


class CREME_MDAO_advanced2(om.Problem):
    """Maximize download by changing orbital parameters. Constrained by altitude, inclination, maximal depth of discharge and link margin
    """
    
    def setup(self):
        # set subsystems
        self.model.add_subsystem('Structure', Structure(), promotes=['capacity']) # to pass the design variable to two different components
        self.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
        self.model.add_subsystem('Power', Power(), promotes=['powerSatTx', 'capacity']) # to pass the design variable to two different components
        self.model.add_subsystem('Data', Data())
        self.model.add_subsystem('Link', Link(), promotes=['powerSatTx']) # to pass the design variable to two different components
        # connect variables
        self.model.connect('Structure.m', 'OrbitalDynamics.m')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMax', 'Link.altitudeMax')
        
        # set up optimization problem
        self.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1.0) # -1.0-> maximize, Mbit,
        # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1) 
        self.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=7200, ref0=6700) # km
        #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
        self.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
        self.model.add_design_var('powerSatTx', lower=0.5, upper=5, ref=1) # W
        self.model.add_design_var('capacity', lower=20, upper=100, ref=10) # Battery capacity Wh

        # set constraints
        [margin_BER_download, margin_BER_upload] = getBERMargins("./data/input.yaml")
        # self.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=250, ref=100) # km
        missionDurationMin = 2.5*365.24 # two and a half year [d]
        tReentryMax = 25*365.24 # maximal alloved stay on orbit - 25 years [d]
        self.model.add_constraint('OrbitalDynamics.OrbitalDynamicsDecay.tReentry', lower=missionDurationMin, upper=tReentryMax, ref=100) # d
        self.model.add_constraint('Power.charge', lower=70, ref=10) # %
        self.model.add_constraint('Link.marginDownload', lower=margin_BER_download) # dB
        self.model.add_constraint('Link.marginUpload', lower=margin_BER_upload) # dB
        # call parent setup
        super().setup()
    
    def configure(self):
        """Used if overwriting of childrens setup function is necessary i.e. assign different solvers
        """
    
    def add_recorder_filename(self, recorder_filename="reports/casesCREME_MDAO_advanced.sql"):
        """Custom function to set up recorder and it's output file

        Args:
            recorder_filename (str, optional): recorder output file. Defaults to "reports/casesCREME_MDAO_advanced.sql".
        """
        # case recorder
        # Create a recorder
        recorder = om.SqliteRecorder(recorder_filename)
        # Attach recorder to the problem
        self.add_recorder(recorder)
        # Attach recorder to the driver
        self.driver.add_recorder(recorder)

class CREME_MDAO_advanced2_GA(om.Problem):
    """Maximize download by changing orbital parameters. Constrained by altitude, inclination, maximal depth of discharge and link margin
        GA stands fo Genetic algorythm. (Differential evolution is also an implementation of a GA)
    """
    
    def setup(self):
        # set subsystems
        self.model.add_subsystem('Structure', Structure(), promotes=['capacity']) # to pass the design variable to two different components
        self.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
        self.model.add_subsystem('Power', Power(), promotes=['powerSatTx', 'capacity']) # to pass the design variable to two different components
        self.model.add_subsystem('Data', Data())
        self.model.add_subsystem('Link', Link(), promotes=['powerSatTx']) # to pass the design variable to two different components
        # connect variables
        self.model.connect('Structure.m', 'OrbitalDynamics.m')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMax', 'Link.altitudeMax')
        
        # set up optimization problem
        self.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1e-2) # -1.0-> maximize, 1e-2 -> To normalize entry to the penalty function: otherwise GA tends to violate constraints, Mbit,
        # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1) 
        self.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=7200, ref0=6700) # km
        #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
        self.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
        self.model.add_design_var('powerSatTx', lower=0.5, upper=5, ref=1) # W
        self.model.add_design_var('capacity', lower=20, upper=100, ref=10) # Battery capacity Wh

        # set constraints
        [margin_BER_download, margin_BER_upload] = getBERMargins("./data/input.yaml")
        # self.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=250, ref=100) # km
        missionDurationMin = 2.5*365.24 # two and a half year [d]
        tReentryMax = 25*365.24 # maximal alloved stay on orbit - 25 years [d]
        self.model.add_constraint('OrbitalDynamics.OrbitalDynamicsDecay.tReentry', lower=missionDurationMin, upper=tReentryMax, ref=10) # d
        self.model.add_constraint('Power.charge', lower=70, ref=10) # %
        self.model.add_constraint('Link.marginDownload', lower=margin_BER_download) # dB
        self.model.add_constraint('Link.marginUpload', lower=margin_BER_upload) # dB
        # call parent setup
        super().setup()
    
    def configure(self):
        """Used if overwriting of childrens setup function is necessary i.e. assign different solvers
        """
    
    def add_recorder_filename(self, recorder_filename="reports/casesCREME_MDAO_advanced.sql"):
        """Custom function to set up recorder and it's output file

        Args:
            recorder_filename (str, optional): recorder output file. Defaults to "reports/casesCREME_MDAO_advanced.sql".
        """
        # case recorder
        # Create a recorder
        recorder = om.SqliteRecorder(recorder_filename)
        # Attach recorder to the problem
        self.add_recorder(recorder)
        # Attach recorder to the driver
        self.driver.add_recorder(recorder)

        
if __name__ == "__main__":
    # Debugging
    prob_CREME = CREME_MDAO_advanced2()
    
    # change top-level solver
    # prob_CREME.model.linear_solver = om.DirectSolver() # not necessary
    
    # set up optimizer
    prob_CREME.driver = om.ScipyOptimizeDriver()
    prob_CREME.driver.options['optimizer'] = 'COBYLA'
    prob_CREME.driver.options['tol'] = 1e-5

    # case recorder
    # Create a recorder
    recorder = om.SqliteRecorder("reports/casesCREME_MDAO_advanced2.sql")
    # Attach recorder to the problem
    prob_CREME.add_recorder(recorder)
    # Attach recorder to the driver
    prob_CREME.driver.add_recorder(recorder)
    
    # seting up the problem
    prob_CREME.setup()

    #check initialization
    print("After initialization")
    print(f"a = {prob_CREME.get_val('OrbitalDynamics.a')}")

    prob_CREME.run_model()
    # check after single solver run
    print("*******After initial solver run:*******")
    prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max', 'lower', 'upper'])
    #display time evolutions
    eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
    charge = prob_CREME.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    # execute optimization
    prob_CREME.run_driver()
    # get results
    print("*******After optimization run:*******")
    prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max', 'lower', 'upper'])
    #display time evolutions
    eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
    charge = prob_CREME.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    plotConvergence("reports/casesCREME_MDAO_advanced2.sql")
    # generate N2
    om.n2(prob_CREME, "N2_diagrams/MDAO/N2-CREME3.html")
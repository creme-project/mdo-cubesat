"""
Optimization runs, including time measurement
"""
import openmdao.api as om
from CREME_MDAO import CREME_MDAO_advanced2_GA
from functionsDatabase import plotEvolutions, plotConvergence
import time

prob_CREME = CREME_MDAO_advanced2_GA()
# change top-level solver
# prob_CREME.model.linear_solver = om.DirectSolver() # not necessary

# set up optimizer
prob_CREME.driver = om.DifferentialEvolutionDriver()
prob_CREME.driver.options['max_gen'] = 100 # Evolutionary
prob_CREME.driver.options['procs_per_model'] = 8 # Evolutionary

# set recorder
prob_CREME.add_recorder_filename("reports/casesCREME_MDAO_advanced2_GA.sql")
# important to set right step!!

# seting up the problem
prob_CREME.setup()

prob_CREME.run_model()
# check after single solver run
print("*******After initial solver run:*******")
prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max', 'lower', 'upper'])
#display time evolutions
eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
charge = prob_CREME.get_val('Power.charge')
plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

# get the start time
st = time.process_time()

# execute optimization
prob_CREME.run_driver()

# get the end time
et = time.process_time()

# get results
print("*******After optimization run:*******")
prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min','max', 'lower', 'upper'])
#display time evolutions
eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
charge = prob_CREME.get_val('Power.charge')
plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

plotConvergence("reports/casesCREME_MDAO_advanced2_GA.sql")

# get execution time
res = et - st
print('CPU Execution time:', res, 'seconds')

# generate N2
om.n2(prob_CREME, "N2_diagrams/MDAO/N2-CREME3.html")


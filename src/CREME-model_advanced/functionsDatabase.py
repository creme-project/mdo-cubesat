import copy
import math
import os
from datetime import datetime
import numpy as np
from matplotlib import pyplot as plt
import yaml
import yamlloader
import openmdao.api as om

"""Functions taken from creme-scripts, some of them are modofied to better fit OpenMDAO implementation of the CubeSat Model
"""

def delUnits(config_dict):

    '''Convert yaml strings inputs with units to float in a config dictionnary

    Args:
        config_dict (dict): input dictionnary loaded from the yaml input file
    
    Returns:
        dict: new_config_without_unit, same dictionnary but without the unit in the values
    '''

    new_config_without_unit = copy.deepcopy(config_dict)
    for key,value in config_dict.items():
        for key_1,value_1 in value.items():
            if type(value_1) is str:
                if '[' in value_1 :
                    new_value = value_1[:-1].split('[')
                    new_config_without_unit[key][key_1] = float(new_value[0])
                else:
                    new_config_without_unit[key][key_1] = value_1
            else:
                new_config_without_unit[key][key_1] = value_1
    return new_config_without_unit

#-----------------------------------------------------------------------------

def to_db(v):
    """convert to decibels """
    return math.log(float(v), 10) * 10

#-----------------------------------------------------------------------------

def writeGMATscript(config, sma, exc, inc, raan, aop, ta, epoch):
    '''Write a new GMAT script with orbit and simulation parameters given. 
    The default GMAT script used is DefaultSat.script file. If you want to change 
    any other parameter than the orbit or the simulation time, open DefaultSat.script in GMAT,
    change the data, and save the script before running this simulation.

    The new GMAT script is saved as MissionFromPython.script in config's GMAT_output_dir

    Args: 
        config (dict): yaml dict form config file to get GS data, simulation total time and timestep
        sma (float): semi-major axis, km
        exc (float): excentricity
        inc (float): inclination, deg
        raan (float): right ascension of the ascending node, deg
        aop (float): argument of periapsis, deg
        ta (float): true anomaly, deg
        epoch (float): epoch as unix timestamp, s
    '''
    #epoch = config["ORBIT"]["epoch"]
    #sma = str(config["ORBIT"]["SMA"])
    #exc = str(config["ORBIT"]["ECC"])
    #inc = str(config["ORBIT"]["INC"])
    #raan = str(config["ORBIT"]["RAAN"])
    #aop = str(config["ORBIT"]["AOP"])
    #ta = str(config["ORBIT"]["TA"])
    tot_time = config['SIMULATION']['total_time']
    GS_Data = {"GS_Altitude": config['GROUNDSTATION_LOCATION']['GS_altitude'],
               "GS_Elevation": config['GROUNDSTATION_LOCATION']['GS_minElevation'],
               "GS_Latitude": config['GROUNDSTATION_LOCATION']['GS_latitude'],
               "GS_Longitude": config['GROUNDSTATION_LOCATION']['GS_longitude']
    }
    dt = config['SIMULATION']['dt']
    
    #Get default CREME data from GMAT script
    fileNameInit = config["paths"]["GMAT_input_dir"]+'DefaultSat.script'
    file_open = open(fileNameInit,'r')
    lines = file_open.readlines()
    file_open.close()

    #replace default orbit data in the GMAT script
    fileNameGmat = config["paths"]["GMAT_input_dir"]+'MissionFromPython.script'
    
    file_open2 = open(fileNameGmat,'w')

    initial_epoch = getGMATepochformat(epoch)

    for u in range(len(lines)):
        if 'GMAT DefaultSat.SMA =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.SMA = ' + str(sma) + ";\n"
        elif 'GMAT DefaultSat.ECC =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.ECC = ' + str(exc) + ";\n"
        elif 'GMAT DefaultSat.INC =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.INC = ' + str(inc) + ";\n"
        elif 'GMAT DefaultSat.RAAN =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.RAAN = ' + str(raan) + ";\n"
        elif 'GMAT DefaultSat.AOP =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.AOP = ' + str(aop) + ";\n"
        elif 'GMAT DefaultSat.TA =' in lines[u] : 
            lines[u] = 'GMAT DefaultSat.TA = ' + str(ta) + ";\n"
        elif 'GMAT DefaultSat.Epoch =' in lines[u] : 
            lines[u] = "GMAT DefaultSat.Epoch = '" + initial_epoch +"';\n"
        elif 'GMAT DefaultGS.Location1 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location1 = " + str(GS_Data["GS_Latitude"]) +";\n"
        elif 'GMAT DefaultGS.Location2 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location2 = " + str(GS_Data["GS_Longitude"]) +";\n"
        elif 'GMAT DefaultGS.Location3 =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.Location3 = " + str(GS_Data["GS_Altitude"]*0.001) +";\n"
        elif 'GMAT DefaultGS.MinimumElevationAngle =' in lines[u] : 
            lines[u] = "GMAT DefaultGS.MinimumElevationAngle = " + str(GS_Data["GS_Elevation"]) +";\n"
        elif 'Propagate Propagator1(DefaultSat)' in lines[u]:
            lines[u] = 'Propagate Propagator1(DefaultSat) {DefaultSat.ElapsedDays = ' + str(tot_time) + '};\n'
        elif 'GMAT Propagator1.InitialStepSize' in lines[u]:
            lines[u] = 'GMAT Propagator1.InitialStepSize = ' + str(dt) + ';\n'
        elif 'GMAT Propagator1.MinStep' in lines[u]:
            lines[u] = 'GMAT Propagator1.MinStep = ' + str(dt) + ';\n'     
        elif 'GMAT Propagator1.MaxStep' in lines[u]:
            lines[u] = 'GMAT Propagator1.MaxStep = ' + str(dt) + ';\n'      
        file_open2.write(lines[u])

    #Close all opened files
    file_open2.close()

#-----------------------------------------------------------------------------

def getGMATepochformat(epoch):
    """Return the given epoch with the format used in GMAT scripts. 

    Args:
       epoch (float): Time epoch, unix timestamp format, s
    
    Returns:
       str: initial epoch, format : DD Mon YYYY HH:MM:SS.mmm
    """
    # TODO - rewrite to get GMAT forlat form unix timestamp, s
    months = ['Jan','Feb','Mar','Apr','May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
    epoch_datetime = datetime.fromtimestamp(epoch)
    initial_epoch = (f"{epoch_datetime.day:02d} {months[int(epoch_datetime.month) - 1]} {epoch_datetime.year} {epoch_datetime.hour:02d}:{epoch_datetime.minute:02d}:{epoch_datetime.second:02d}.000") # with precision to seconds, as in the input.yaml
    
    return initial_epoch

#-----------------------------------------------------------------------------
        
#-----------------------------------------------------------------------------

def detectEvents(dataFilePath, initial_epoch):
    """Detect events from a GMAT locator report (eclipses or contacts).
    Locator reports are .txt files automatically generated by GMAT.
    Please refer to the DefaultSat.script file to see how they are configured in the GMAT app. 

    Args: 
        dataFilePath (str): Path to a GMAT event text file report (contactLocator or eclipseLocator report)
        initial_epoch (float): Initial GMAT simulation epoch as a unix timastamp, s 

    Returns:
        list: list containing:

            float array: sec, time array to plot the event occurences

            bool array: events, boolean array true = event is here, false = no event

            float: tot_time_event, the last time in the GMAT file for checking

            bool: true if no event occurs for aditional fool-proof-ing

    sec and events are constructed so that you can plot the event occurences easily (Example : plot(sec,events))
    """
    #Read data from file
    f = open(dataFilePath, "r")
    lines = f.readlines()
    f.close()
    #init variables
    init_seconds = 0.0
    no_event = False
    tot_time_event = 0
    sec = [init_seconds]
    events = [False]
    #detect first date line
    found_first_line = False
    cpt = 0 # line indicator
    while not found_first_line and cpt < len(lines):
        if "Start Time" in lines[cpt]:
            found_first_line = True
        else:
            cpt = cpt + 1
    #compute events array to be plotted
    if not found_first_line:
        no_event = True
        tot_time_event = 0
    else:
        for l in lines[cpt+1:] :
            line_split = l.split("    ")
            if len(line_split) > 3:
                date_0 = datetime.strptime(line_split[0], '%d %b %Y %H:%M:%S.%f').timestamp() # unix timestamp, s
                date_1 = datetime.strptime(line_split[1], '%d %b %Y %H:%M:%S.%f').timestamp() # unix timestamp, s
                ds0 = (date_0-initial_epoch)
                ds1 = (date_1-initial_epoch)
                sec.append(ds0)
                sec.append(ds0)
                sec.append(ds1)
                sec.append(ds1)
                events.append(False)
                events.append(True)
                events.append(True)
                events.append(False)
                tot_time_event = ds1  
    sec = np.array(sec)
    events = np.array(events)
    return [sec, events, tot_time_event, no_event]

#-----------------------------------------------------------------------------

def parseEvents(eventData, tTotal, dt):
    """Get outputs of the detectEvents function and return array on a per-dt basis.

    Args:
        eventData (list): List containing:
            [0] (array float): times of change of event state, s
            [1] (array boolean): Event occurence array, True - visible, False - not visible
            [2] (boolean): True if no event was detected
        tTotal (float): total time of simulation, s
        dt (float): timestep for events evolution, s
        
    Returns:
        float array: eventEvolutiuon, Event occurence each dt, binary {0,1}
    """
    
    secEvent = eventData[0]
    eventOccurrence = eventData[1]
    noEvent = eventData[3]
    
    numberOfSteps = round(tTotal/dt)
    eventEvolution = np.zeros(numberOfSteps) # 0: Charge 1 : Mesure 2 : Vidage station
    
    if not noEvent:
        # Find event occurences
        # Note, since round() is not very fast, a different approach is used
        # int(secEvent[i]/dt + 0.5) is equivalent to round(secEvent[i]/dt, 0)
        for i in range(len(secEvent)-2):
            if eventOccurrence[i] == True and eventOccurrence[i+1] == True:
                eventEvolution[int(secEvent[i]/dt + 0.5) : int(secEvent[i+1]/dt + 0.5)] = 1
        
    return eventEvolution

#-----------------------------------------------------------------------------
def plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, configFileName="./data/input.yaml"):
    """Plots time evolution of the time-dependent variables
    TODO - finish description
    Args:
        eclipses (_type_): _description_
        gsVisibility (_type_): _description_
        downlinkCumulative (_type_): _description_
        downlinkRate (_type_): _description_
        charge (_type_): _description_
        configFileName (str, optional): _description_. Defaults to "./data/input.yaml".
    """
    # load input.yaml file into config variable 
    stream = open(configFileName, 'r')
    config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
    stream.close()
    config = delUnits(config)
    tTotal = float(config['SIMULATION']['total_time']) * 86400 # mean synodic day to sec
    dt = float(config['DATA_BUDGET']['dt']) # Important! take dt from DATA_BUDGET bacause it is has a different value from dt in SIMULATION
    
    # plotting
    time = np.arange(0, round(tTotal/dt), dt)
    fig, axs = plt.subplots(3)
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
    
    lineEclipses, = axs[0].plot(time, eclipses, 'k', label="Eclipses")
    lineGSVisibility,  = axs[0].plot(time, gsVisibility, 'g', label="GS visibility")
    axs[0].set(xlabel='Time [s]', ylabel='True/False [-]', title="Eclipses and Visibility")
    axs[0].grid()
    axs[0].legend(handles=[lineEclipses, lineGSVisibility], bbox_to_anchor= (0.95, 1.35), loc='upper right')
    
    lineDownCumul,  = axs[1].plot(time, downlinkCumulative, 'k', label="Cumulative")
    axs[1].set(xlabel='Time [s]', ylabel='Download cumulative [Mbit]', title="Download")
    ax2=axs[1].twinx()
    lineDownRate, = ax2.plot(time, downlinkRate, 'b', label="Rate")
    ax2.set(ylabel='Download rate [bit/s]')
    axs[1].legend(handles=[lineDownCumul, lineDownRate], bbox_to_anchor= (0.95, 1.35), loc='upper right')
    
    axs[2].plot(time, charge)
    axs[2].set(xlabel='Time [s]', ylabel='Battery charge state [%]', title="Power")
    plt.show()

#-----------------------------------------------------------------------------
def plotConvergence(casesArchiveFilename):
    # Instantiate your CaseReader
    cr = om.CaseReader(casesArchiveFilename)

    # Get driver cases (do not recurse to system/solver cases)
    driver_cases = cr.get_cases('driver', recurse=False)

    # Plot the path the design variables took to convergence
    # Note that there are two lines in the right plot because "Z"
    # contains two variables that are being optimized
    objective_DownloadTotal_values = []
    desVar_a_values = []
    desVar_inc_values = []
    desVar_powerSatTx_values = []
    for case in driver_cases:
        objective_DownloadTotal_values.append(case['Data.DataDownload.downlinkTotal'])
        desVar_a_values.append(case['OrbitalDynamics.a'])
        desVar_inc_values.append(case['OrbitalDynamics.inc'])
        desVar_powerSatTx_values.append(case['powerSatTx'])

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1)
    fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)

    ax1.plot(np.arange(len(objective_DownloadTotal_values)), np.array(objective_DownloadTotal_values))
    ax1.set(xlabel='Iterations', ylabel='Objective: downlinkTotal [Mbit]', title='Optimization History')
    ax1.grid()

    ax2.plot(np.arange(len(desVar_a_values)), np.array(desVar_a_values))
    ax2.set(xlabel='Iterations', ylabel='Design Var: a [km]')
    ax2.grid()
    
    ax3.plot(np.arange(len(desVar_inc_values)), np.array(desVar_inc_values))
    ax3.set(xlabel='Iterations', ylabel='Design Var: inclination [°]')
    ax3.grid()

    ax4.plot(np.arange(len(desVar_powerSatTx_values)), np.array(desVar_powerSatTx_values))
    ax4.set(xlabel='Iterations', ylabel='Design Var: P_sc_tx [W]')
    ax4.grid()
    
    plt.show()

#-----------------------------------------------------------------------------
def getBERMargins(configFileName):
    
    stream = open(configFileName, 'r')
    config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
    stream.close()
    config = delUnits(config)
    
    margin_BER_download = float(config['DOWNLINK']['required_BER_from_Modulation']) # dB
    margin_BER_upload = float(config['UPLINK']['required_BER_from_Modulation']) # dB
    
    return margin_BER_download, margin_BER_upload
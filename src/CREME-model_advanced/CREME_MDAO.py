# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

MDAO ipmlementation of the CREME optimization problem
"""
from matplotlib import pyplot as plt
import openmdao.api as om
from DisciplinesAnalyticPartials import *
from functionsDatabase import getBERMargins, plotEvolutions, plotConvergence

class CREME_MDAO_advanced1(om.Problem):
    """Maximize download by changing orbital parameters. Constrained by altitude, inclination, maximal depth of discharge and link margin
    """
    
    def setup(self):
        # set subsystems
        self.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
        self.model.add_subsystem('Data', Data())
        self.model.add_subsystem('Link', Link(), promotes=['powerSatTx']) # to pass the design variable to two different components
        self.model.add_subsystem('Power', Power(), promotes=['powerSatTx']) # to pass the design variable to two different components
        # connect variables
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
        self.model.connect('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMax', 'Link.altitudeMax')
        
        # set up optimization problem
        self.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1e-2) # -1.0-> maximize, 1e-2 -> for penalisation when running constrained GA, Mbit,
        # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1)
        self.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=1000) # km
        #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
        self.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
        self.model.add_design_var('powerSatTx', lower=0.5, upper=5, ref=1) # W

        # set constraints
        [margin_BER_download, margin_BER_upload] = getBERMargins("./data/input.yaml")
        self.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=450, ref=100) # km
        self.model.add_constraint('Power.charge', lower=70, ref=10) # %
        self.model.add_constraint('Link.marginDownload', lower=margin_BER_download) # dB
        self.model.add_constraint('Link.marginUpload', lower=margin_BER_upload) # dB
        # call parent setup
        super().setup()
    
    def configure(self):
        """Used if overwriting of childrens setup function is necessary i.e. assign different solvers
        """
    
    def add_recorder_filename(self, recorder_filename="reports/casesCREME_MDAO_advanced.sql"):
        """Custom function to set up recorder and it's output file

        Args:
            recorder_filename (str, optional): recorder output file. Defaults to "reports/casesCREME_MDAO_advanced.sql".
        """
        # case recorder
        # Create a recorder
        recorder = om.SqliteRecorder(recorder_filename)
        # Attach recorder to the problem
        self.add_recorder(recorder)
        # Attach recorder to the driver
        self.driver.add_recorder(recorder)
        
if __name__ == "__main__":
    # Debugging
    prob_CREME = CREME_MDAO_advanced1()
    
    # change top-level solver
    # prob_CREME.model.linear_solver = om.DirectSolver() # not necessary
    
    # set up optimizer
    prob_CREME.driver = om.ScipyOptimizeDriver()
    prob_CREME.driver.options['optimizer'] = 'COBYLA'
    prob_CREME.driver.options['tol'] = 1e-5

    # case recorder
    # Create a recorder
    recorder = om.SqliteRecorder("reports/casesCREME_MDAO_advanced.sql")
    # Attach recorder to the problem
    prob_CREME.add_recorder(recorder)
    # Attach recorder to the driver
    prob_CREME.driver.add_recorder(recorder)
    
    # seting up the problem
    prob_CREME.setup()

    #check initialization
    print("After initialization")
    print(f"a = {prob_CREME.get_val('OrbitalDynamics.a')}")

    prob_CREME.run_model()
    # check after single solver run
    print("*******After initial solver run:*******")
    prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max'])
    #display time evolutions
    eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
    charge = prob_CREME.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    # execute optimization
    prob_CREME.run_driver()
    # get results
    print("*******After optimization run:*******")
    prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min','max'])
    #display time evolutions
    eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
    charge = prob_CREME.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    plotConvergence("reports/casesCREME_MDAO_advanced.sql")
    # generate N2
    om.n2(prob_CREME, "N2_diagrams/MDAO/N2-CREME2.html")
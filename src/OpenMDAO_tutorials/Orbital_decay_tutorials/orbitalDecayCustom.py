from ast import For
import numpy as np


def lifetimeOnCircularOrbit(a0, S, m, Cd=2.3, dt=86400, t_max=26*365.24*24*3600):
    """Calculates orbital decay time for a circular orbit. If t of the decay surpases t_max, the calculation stops and returns the latest t value
        Hypothesis: Atmospheric density depends exponentially on the altitude
        
        Inputs:
            - a0 (float): Semimajpr axis [km]
            - S (float): Cross section [m^2]
            - Cd (float): drag coefficient [-]
            - dt (float): Integration step [s]
            - t_max (float): Decay time limit [s]
            
        Outputs
            - float: t, time of decay [s]
    """ 
    mu = 398600 # km^3
    h0 = 200 # km
    rho0 = 1e-10 # kg/m^3
    alpha = 0.016 # 1/km
    
    t = 0 # s
    a = a0 # km
    h = a - 6378.136 # km
    # for i in range(10000):
    while h > 0.0 :
        v = np.sqrt(mu/a) # km/s    
        rho = rho0 * np.exp(-alpha*(h-h0)) # kg/m^3
        gamma = 1.0/2.0 * Cd * S * rho * v**2 / m
        da_dt = -2 * a**2 * v * gamma / mu * 10**3 # km/s
        
        a = a + da_dt*dt # km
        h = a - 6378.136 # km
        t = t + dt # s
        if t > t_max:
            break
    
    return t

if __name__ == "__main__":
    t_vie = lifetimeOnCircularOrbit(6378+800, 0.034, 4, 2.3)/86400/365.24
    print(t_vie)
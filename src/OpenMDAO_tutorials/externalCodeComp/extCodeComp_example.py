"""Tutorial on using ExternalCodeComp API from OpenMDAO documentation site https://openmdao.org/newdocs/versions/latest/features/building_blocks/components/external_code_comp.html?highlight=externalcodecomp
"""

import openmdao.api as om


class ParaboloidExternalCodeComp(om.ExternalCodeComp):
    def setup(self):
        self.add_input('x', val=0.0)
        self.add_input('y', val=0.0)

        self.add_output('f_xy', val=0.0)

        self.input_file = 'paraboloid_input.dat'
        self.output_file = 'paraboloid_output.dat'

        # providing these is optional; the component will verify that any input
        # files exist before execution and that the output files exist after.
        self.options['external_input_files'] = [self.input_file]
        self.options['external_output_files'] = [self.output_file]

        # If you want to write your command as a list, the code below will also work.
        # self.options['command'] = [
        #     sys.executable, 'extcode_paraboloid.py', self.input_file, self.output_file
        # ]

        self.options['command'] = ('python extcode_paraboloid.py {} {}').format(self.input_file, self.output_file)

    def compute(self, inputs, outputs):
        x = inputs['x']
        y = inputs['y']

        # generate the input file for the paraboloid external code
        with open(self.input_file, 'w') as input_file:
            input_file.write('%.16f\n%.16f\n' % (x, y))

        # the parent compute function actually runs the external code
        super().compute(inputs, outputs)

        # parse the output file from the external code and set the value of f_xy
        with open(self.output_file, 'r') as output_file:
            f_xy = float(output_file.read())

        outputs['f_xy'] = f_xy
        
# ----------------execution-------------------------
import sys

prob = om.Problem()
model = prob.model

model.add_subsystem('p', ParaboloidExternalCodeComp(), promotes_inputs=['x', 'y'])

# run the ExternalCodeComp Component
prob.setup()

# Set input values
prob.set_val('p.x', 3.0)
prob.set_val('p.y', -4.0)

prob.run_model()

# print the output
print(prob.get_val('p.f_xy'))
# -*- coding: utf-8 -*-
"""
Library of sample components to compare different classes of solvers
"""

import openmdao.api as om

class ExplicitDisciplineParaboloid(om.ExplicitComponent):
    """
    Evaluates the equation f(x,y) = (x-3)^2 + xy + (y+4)^2 - 3.
    """

    def setup(self):
        self.add_input('x', val=0.0)
        self.add_input('y', val=0.0)

        self.add_output('f_xy', val=0.0)

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """
        f(x,y) = (x-3)^2 + xy + (y+4)^2 - 3

        Minimum at: x = 6.6667; y = -7.3333
        """
        x = inputs['x']
        y = inputs['y']

        outputs['f_xy'] = (x - 3.0)**2 + x * y + (y + 4.0)**2 - 3.0
        
class ImplicitDisciplineParaboloid(om.ImplicitComponent):
    """
    Evaluates the equation 0 = (x-3)^2 + xy + (y+4)^2 - 3 - f(x,y).
    Note - To use analytical derivatives a linearize(inputs, outputs, partials) method has to be used
    """
    
    def setup(self):
        self.add_input('x', val=0.0)
        self.add_input('y', val=0.0)
        
        self.add_output('f_xy', val=0.0)
    
    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('f_xy', ['x','y','f_xy'], method='fd')
    
    def apply_nonlinear(self, inputs, outputs, residuals):
        x = inputs['x']
        y = inputs['y']
        f = outputs['f_xy']
        residuals['f_xy'] = ((x - 3.0)**2 + x * y + (y + 4.0)**2 - 3.0) - f
    
    def linearize(self, inputs, outputs, partials):
        x = inputs['x']
        y = inputs['y']
        partials['f_xy', 'x'] = 2*(x-3) + y
        partials['f_xy', 'y'] = x + 2*(y+4)
        partials['f_xy', 'f_xy'] = -1

class ExplicitDiscip1(om.ExplicitComponent):
    """
    Evaluates the equation y(x,z) = -3z^2 + 3. (hyperbolical paraboloid)
    """

    def setup(self):
        self.add_input('z', val=0.0)

        self.add_output('y', val=0.0)

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """
        y(x) = -3z^2 + 3
        """
        z = inputs['z']

        outputs['y'] = -3 * z**2 + 3.0
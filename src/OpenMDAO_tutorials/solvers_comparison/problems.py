# -*- coding: utf-8 -*-
"""
Library of sample problems to compare different classes of solvers. Uses sample problems from disciplines.py 
"""
import openmdao.api as om
from disciplines import *

def runExplicitSimpleProblem():
    # build the model
    prob = om.Problem()
    prob.model.add_subsystem('ExplicitParab', ExplicitDisciplineParaboloid())

    # Design variables 'x' and 'y' span components, so we need to provide a common initial
    # value for them.
    prob.model.set_input_defaults('ExplicitParab.x', 3.0)
    prob.model.set_input_defaults('ExplicitParab.y', -4.0)

    # analysis
    prob.model.linear_solver = om.LinearRunOnce()
    # prob.model.nonlinear_solver = om.NonlinearRunOnce() # No information in the OpenMDAO documentation, but seems to work 

    # setup the optimization
    # prob.driver = om.ScipyOptimizeDriver()
    # prob.driver.options['optimizer'] = 'COBYLA'

    # prob.model.add_design_var('ExplicitParab.x', lower=-50, upper=50)
    # prob.model.add_design_var('ExplicitParab.y', lower=-50, upper=50)
    # prob.model.add_objective('ExplicitParab.f_xy')

    prob.setup()
    prob.run_model()
    prob.model.list_inputs()
    prob.model.list_outputs()

    # prob.run_driver()
    
    # prob.model.list_inputs()
    # prob.model.list_outputs()
#-------------------------------------------------------------------------------------------------------------------------------------- 
def runImplicitSimpleProblem():
    # build the model
    prob = om.Problem()
    prob.model.add_subsystem('ImplicitParab', ImplicitDisciplineParaboloid())

    # Design variables 'x' and 'y' span components, so we need to provide a common initial
    # value for them.
    prob.model.set_input_defaults('ImplicitParab.x', 3.0)
    prob.model.set_input_defaults('ImplicitParab.y', -4.0)

    # analysis
    prob.model.nonlinear_solver = om.NewtonSolver(solve_subsystems=False, maxiter=100) # works fine without linearize method defined and 
    # with LinearRunOnce solver, but it has problems when using RunOnce and the residuals are defined with opposite signs. Works okay with DirectSolver
    # prob.model.nonlinear_solver = om.NonlinearBlockGS(maxiter=5) # should work for coupled systelms, but without implicit states
    # prob.model.nonlinear_solver = om.BroydenSolver() # works fine with DirectSolver
    # prob.model.linear_solver = om.LinearRunOnce() # default one
    prob.model.linear_solver = om.DirectSolver() # this one needs linearize function to be defined

    # setup the optimization
    prob.driver = om.ScipyOptimizeDriver()
    prob.driver.options['optimizer'] = 'COBYLA'

    prob.model.add_design_var('ImplicitParab.x', lower=-50, upper=50)
    prob.model.add_design_var('ImplicitParab.y', lower=-50, upper=50)
    prob.model.add_objective('ImplicitParab.f_xy')

    prob.setup()
    prob.run_model()
    prob.model.list_inputs()
    prob.model.list_outputs()

    prob.run_driver()
    prob.model.list_inputs()
    prob.model.list_outputs()

#--------------------------------------------------------------------------------------------------------------------------------------
def runExplicitCoupledleProblem():
    #Note - this example corresponds to the sellar problem https://openmdao.org/newdocs/versions/latest/basic_user_guide/multidisciplinary_optimization/sellar.html
    # build the model
    prob = om.Problem()
    prob.model.add_subsystem('ExplicitDiscip1', ExplicitDiscip1())
    prob.model.add_subsystem('ExplicitParab', ExplicitDisciplineParaboloid())
    #connecting coupled disciplines
    prob.model.connect('ExplicitDiscip1.y', 'ExplicitParab.y')
    prob.model.connect('ExplicitParab.f_xy', 'ExplicitDiscip1.z')
    
    # Design variables 'x' and 'y' span components, so we need to provide a common initial
    # value for them.
    prob.model.set_input_defaults('ExplicitParab.x', 3.0)
    prob.model.set_input_defaults('ExplicitParab.y', -4.0)

    # analysis
    prob.model.nonlinear_solver = om.NewtonSolver(solve_subsystems=False, maxiter=100) # works fine, but needs the DirectSolver
    # with LinearRunOnce solver, but it has problems when using RunOnce and the residuals are defined with opposite signs. Works okay with DirectSolver
    # prob.model.nonlinear_solver = om.NonlinearBlockGS(maxiter=20) # Diverges
    # prob.model.nonlinear_solver = om.BroydenSolver(maxiter=20) # Doesn't converge, may require linesearch solver since it takes too large steps
    # prob.model.linear_solver = om.LinearRunOnce() # default one
    prob.model.linear_solver = om.DirectSolver() # this one needs linearize function to be defined
    # using only BASIC DirectSolver doesn't converge
    # prob.model.linear_solver = om.ScipyKrylov() # diverges

    # setup the optimization
    # prob.driver = om.ScipyOptimizeDriver()
    # prob.driver.options['optimizer'] = 'COBYLA'

    # prob.model.add_design_var('ExplicitParab.x', lower=-50, upper=50)
    # prob.model.add_objective('ExplicitParab.f_xy')

    prob.setup()
    # prob.set_solver_print(level=2)
    prob.run_model() # may serve also as a pre-initialisation/pre-conditioning
    prob.model.list_inputs()
    prob.model.list_outputs()

    # prob.run_driver()
    # prob.model.list_inputs()
    # prob.model.list_outputs()
# NOTE an explicit-implicit coupled problem is presented in "OpenMDAO: an open-source framework for multidisciplinary design, analysis and optimization" chapter 3.5
#============================Execution==========================

if __name__ == '__main__':
    # runExplicitSimpleProblem()
    runImplicitSimpleProblem()
    # runExplicitCoupledleProblem()
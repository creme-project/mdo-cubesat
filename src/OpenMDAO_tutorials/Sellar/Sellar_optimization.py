"""Tutorial on running optimization on multidisciplinary coupled problem https://openmdao.org/newdocs/versions/latest/basic_user_guide/multidisciplinary_optimization/sellar_opt.html
"""

import openmdao.api as om
from Disciplines import SellarMDA


# added by user
from omxdsm import write_xdsm

prob = om.Problem()
prob.model = SellarMDA()

prob.driver = om.ScipyOptimizeDriver()
prob.driver.options['optimizer'] = 'SLSQP'
# prob.driver.options['maxiter'] = 100
prob.driver.options['tol'] = 1e-8

prob.model.add_design_var('x', lower=0, upper=10)
prob.model.add_design_var('z', lower=0, upper=10)
prob.model.add_objective('obj')
prob.model.add_constraint('con1', upper=0)
prob.model.add_constraint('con2', upper=0)

# Ask OpenMDAO to finite-difference across the model to compute the gradients for the optimizer
prob.model.approx_totals()  # it is preferable to use  "semi-analytic" technique

prob.setup()
prob.set_solver_print(level=0) # print msg only if the solver fails

prob.run_driver()

print('minimum found at')
print(prob.get_val('x')[0])
print(prob.get_val('z'))

print('minumum objective')
print(prob.get_val('obj')[0])

# OpenMDAO-XDSM test, added by user
# Write output. PDF will only be created, if pdflatex is installed
write_xdsm(prob, filename='Sellar_XDSM_pyxdsm', out_format='html', show_browser=True,
            quiet=False, output_side='left')
# it seems to work

from openmdao.test_suite.test_examples.beam_optimization.beam_group import BeamGroup
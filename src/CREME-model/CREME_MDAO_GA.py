# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

MDAO ipmlementation of the CREME optimization problem
TODO - implement optimization
"""
from matplotlib import pyplot as plt
import openmdao.api as om
from omxdsm import write_xdsm
from DisciplinesAnalyticPartials import *
from functionsDatabase import getBERMargins, plotEvolutions, plotConvergence
import time



"""Maximize download by changing orbital parameters. Constrained by altitude, inclination, maximal depth of discharge and link margin
"""
prob_CREME = om.Problem()
prob_CREME.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
prob_CREME.model.add_subsystem('Data', Data())
prob_CREME.model.add_subsystem('Link', Link())
prob_CREME.model.add_subsystem('Power', Power())

# default selver used

# set up optimizer
prob_CREME.driver = om.DifferentialEvolutionDriver()
prob_CREME.driver.options['max_gen'] = 5
prob_CREME.driver.options['procs_per_model'] = 6

# set up optimization problem
prob_CREME.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1e-2) # -1.0-> maximize, 1e-2 - attention for scaling; otherwise the penalty doesnt work too great.Mbit,
# using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1)
prob_CREME.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=1000) # km
#prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
prob_CREME.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg

# set constraints
[margin_BER_download, margin_BER_upload] = getBERMargins("./data/input.yaml")
prob_CREME.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=450, ref=100) # km
prob_CREME.model.add_constraint('Power.charge', lower=70, ref=10) # %
prob_CREME.model.add_constraint('Link.marginDownload', lower=margin_BER_download) # dB
prob_CREME.model.add_constraint('Link.marginUpload', lower=margin_BER_upload) # dB

prob_CREME.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
prob_CREME.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
prob_CREME.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
prob_CREME.model.connect('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMax', 'Link.altitudeMax')

# prob_Link.set_solver_print(level=0) # print msg only if the solver fails

# case recorder
# Create a recorder
recorder = om.SqliteRecorder("reports/casesCREME_MDAO_GA.sql")

# Attach recorder to the problem
prob_CREME.add_recorder(recorder)
# Attach recorder to the driver
prob_CREME.driver.add_recorder(recorder)

# seting up the problem
prob_CREME.setup()

#check initialization
print("After initialization")
print(f"a = {prob_CREME.get_val('OrbitalDynamics.a')}")

prob_CREME.run_model()
# check after single solver run
print("*******After initial solver run:*******")
prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max'])
#display time evolutions
eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
charge = prob_CREME.get_val('Power.charge')
plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

# get the start time
st = time.process_time()

# execute optimization
prob_CREME.run_driver()

# get the end time
et = time.process_time()

# get results
print("*******After optimization run:*******")
prob_CREME.list_problem_vars(driver_scaling=False, cons_opts=['min','max'])
#display time evolutions
eclipses = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
gsVisibility = prob_CREME.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
charge = prob_CREME.get_val('Power.charge')
plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

plotConvergence("reports/casesCREME_MDAO_GA.sql")

# get execution time
res = et - st
print('CPU Execution time:', res, 'seconds')

# generate N2
om.n2(prob_CREME, "N2_diagrams/MDAO/N2-CREME1_GA.html")
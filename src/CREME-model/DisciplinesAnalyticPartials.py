# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

Implementation of the CREM CubeSat Model
Note: abbreviations "gs" and "GS" stand for "ground station", "sc" stands for "spacecraft" i.e. satellite
This file contains a simple model of CREME with analytic partial derivatives
"""

from datetime import datetime
import openmdao.api as om

import numpy as np
import yaml
import yamlloader

from functionsDatabase import delUnits, to_db, writeGMATscript, detectEvents, parseEvents

#------------------------------------------------------------------------------------------------------------
class OrbitalDynamicsVisibility(om.ExternalCodeComp):
    """Compute orbit-dependent variables.
    
    Inputs :
        - a (float): Semimajor axis [km]
        - exc (float): Excenticity
        - inc (float): Inclination [deg]
        - raan (float): Right ascension of the ascending node [deg]
        - aop (float): Argument of periapsis [deg]
        - ta (float): True anomaly [deg]
        - t0 (float): Epoch - unix timestamp [s]
    constants :
        - Ground stations position informations from input.yaml
        - total_time: Total length of the GMAT simulation, [d]
        - dt: timestep for GMAT calculation [s]
    outputs :
        - float array: eclipses, List of eclipse state every time step {0,1}
        - float array: gsVisibility, List of ground station visibility every time step {0,1}
    """  
    
    def setup(self):
        # load input.yaml file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        
        # load constants
        self.tTotal = float(self.config['SIMULATION']['total_time']) * 86400 # mean synodic day to sec
        self.dt = float(self.config['DATA_BUDGET']['dt']) # Important! take dt from DATA_BUDGET bacause it is has a different value from dt in SIMULATION
        numberOfSteps = round(self.tTotal/self.dt)
        a = float(self.config['ORBIT']['SMA']) # km
        # load GMAT and its input and output files
        self.main_dir = self.config['paths']['main_dir'] # attention on the space in the path
        self.GMAT_bin = self.config['paths']['GMAT_BIN_linux']
        self.GMAT_inputFile = self.main_dir + self.config['paths']['GMAT_input_dir'] + "MissionFromPython.script" # GMAT reads input form here
        # NOTE - GMAT output is specified in the MissionFromPython.script
        
        # declare inputs
        self.add_input('a', val=a) #SMA [km]
        self.add_input('exc', val=0.0) # excentricity
        self.add_input('inc', val=98.0) # inclination [deg]
        self.add_input('raan', val=270) # raan [deg]
        self.add_input('aop', val=90.0) # argument of periapsis [deg]
        self.add_input('ta', val=270.0) # true anomaly [deg]
        self.add_input('t0', val=datetime(2023,1,1,0,0,0).timestamp()) # epoch - unix timestamp [s]
        # declare outputs
        self.add_output('eclipses', shape=(numberOfSteps, ))
        self.add_output('gsVisibility', shape=(numberOfSteps, ))
        
        # GMAT execute cmd        
        # IMPORTANT - super.compute(...) execution in self.compute(...) fails unless the command is specified as a list
        self.options['command'] = [
            f"{self.GMAT_bin}",
            "--run",
            f"{self.GMAT_inputFile}"
        ]
        
    def compute(self, inputs, outputs):
        # load variables
        a = inputs['a'][0]
        exc = inputs['exc'][0]
        inc = inputs['inc'][0]
        raan = inputs['raan'][0]
        aop = inputs['aop'][0]
        ta = inputs['ta'][0]
        t0 = inputs['t0'][0]
        
        # generate GMAT input file; config to get GS data
        # avoiding negative excentricity
        if exc < 0.0:
            exc=0
        writeGMATscript(self.config, a, exc, inc, raan, aop, ta, t0)
        
        # run GMAT using ExternalComp api
        super().compute(inputs, outputs)
        
        # Extract results form GMAT output file
        eclipseData = detectEvents(self.config['paths']['GMAT_output_dir']+"EclipseLocator1.txt", t0)
        visibilityData = detectEvents(self.config['paths']['GMAT_output_dir']+"ContactLocator1.txt", t0)
        # change to per-dt array
        eclipses = parseEvents(eclipseData, self.tTotal, self.dt)
        gsVisibility = parseEvents(visibilityData, self.tTotal, self.dt)
        
        outputs['eclipses'] = eclipses
        outputs['gsVisibility'] = gsVisibility
        
    def setup_partials(self):
        # Finite difference partials.
        self.declare_partials(of=['eclipses','gsVisibility'], wrt=['a','exc','inc'], method='fd')
        
#------------------------------------------------------------------------------------------------------------
class OrbitalDynamicsAltitude(om.ExplicitComponent):
    """Compute maximal altitude as a function of orbit variables.
    
    Inputs :
        - a (float): Semimajor axis [km]
        - exc (float): Excenticity
    outputs :
        - float: altitudeMax, Maximal altitude under the spherical Earth hypotheis [km]
    """  
    def setup(self):
        # load input.yaml file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        
        a = float(self.config['ORBIT']['SMA']) # km
        # declare inputs
        self.add_input('a', val=a) #SMA [km]
        self.add_input('exc', val=0.0) # excentricity
        # declare outputs
        self.add_output('altitudeMax', val=0) # [km]
        self.add_output('altitudeMin', val=0) # [km]
        
    def compute(self, inputs, outputs):
        # load variables
        a = inputs['a'][0]
        exc = inputs['exc'][0]
        
        outputs['altitudeMax'] = a*(1 + exc) - 6378.136 # [km]
        outputs['altitudeMin'] = a*(1 - exc) - 6378.136 # [km]
        
    def setup_partials(self):
        # Anaytical partials
        self.declare_partials(of=['altitudeMax','altitudeMin'], wrt=['a','exc'])
    
    def compute_partials(self, inputs, partials):
        a = inputs['a']
        exc = inputs['exc']
        
        partials['altitudeMax', 'a'] = 1 + exc
        partials['altitudeMax', 'exc'] = a
        
        partials['altitudeMin', 'a'] = 1 - exc
        partials['altitudeMin', 'exc'] = a
    
#------------------------------------------------------------------------------------------------------------
class Power(om.ExplicitComponent):
    """Compute Battery DOD over the simulation period.

    Hypothesis : 
        -  3 modes are considered for the nanosat: Charge, Mesure (nominal), Vidage
        -  All eclipses are considered as dark (umbra)
        -  During charging, the satellite continues to mesure (the power for mode Mesure is needed during charging)

    Inputs :        
        - eclipses (list): List of eclipse state every time step, {0,1}
        - gsVisibility (list): List of ground station visibility every time step, {0,1}
    Constants :
        - dt (float): time step [sec]
        - totBatPower (float): Total battery energy [Wh]
        - solarPower (float): Power given by the solar panels [W]
        - PMeasure (float): Power consumed during the mesure mode [W]
        - PVidage (float): Power consumed during the vidage mode [W]
    Outputs :
        - float array: charge, Remaining power in the battery at each timestep during simulation [%] - constrained by DOD_max
        - int array: mode, describe satellite mode at each second (0: Charge; 1 : Measure; 2 : Vidage station) - just for information and plotting
    """

    def initialize(self):
        # set constants in the problem
        # load input file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        # time step
        self.dt = float(self.config['POWER_BUDGET']['dt']) # time step in the data arrays, s
        
        # design constants
        self.totBatPower = float(self.config['POWER_BUDGET']['TotBatPower']) # Energy when fully charged, Wh
        self.solarPower = float(self.config['POWER_BUDGET']['SolarPower']) # Sollar cells power, W
        self.PMeasure = float(self.config['POWER_BUDGET']['PMeasure']) # Power consumption of the satellite in the measurement mode, W
        self.PVidage = float(self.config['POWER_BUDGET']['PVidage']) # Power consumption of the satellite in during a data exchange, W
        
        
    def setup(self):
        #input variables
        self.add_input('eclipses', shape_by_conn=True) # 0, 1 - False, True
        self.add_input('gsVisibility', shape_by_conn=True) # 0, 1 - False, True
        # possible variables for more coherent model
        #self.add_input('batteryCap', val=0)
        #self.add_input('panelsPower', val=0)
        #self.add_input('dischargePower', val=[0])
        
        #output variables
        self.add_output('charge', copy_shape='eclipses') # Charge level of teh battery, %
        self.add_output('mode', copy_shape='eclipses') # For information, checking and plotting, int array, {0,1,2}
        
    def compute(self, inputs, outputs):
        eclipses = inputs['eclipses'] # array, 0, 1
        gsVisibility = inputs['gsVisibility'] # array, 0, 1
        
        mode = np.zeros(len(eclipses)) # 0: Charge 1 : Measure 2 : Vidage station

        # Deside the mode of operation, eclipses first, GS contact on top of them
        for i in range(len(eclipses)):
            if eclipses[i] == 1:
                mode[i] = 1
                
        # GS contact
        for i in range(len(gsVisibility)):
            if gsVisibility[i] == 1:
                mode[i] = 2
        
        #compute remaining power
        charge = np.zeros(len(eclipses)) # charge evolution, %
        charge[0] = self.totBatPower # we assume a fully charged battery at the beginning
        ChargeLastVal = self.totBatPower
        vidageCoeff = self.PVidage/3600.0*self.dt # to get from W to Wh by the fact that timestep unit here is in seconds
        measureCoeff = self.PMeasure/3600.0*self.dt
        chargeCoeff = (self.solarPower - self.PMeasure)/3600.0*self.dt
        
        for i in range(len(mode)-1): # ommiting the last dt to keep the same array length
            if mode[i] == 0:  # charge, i.e. solar panels active + taking measurements
                if ChargeLastVal < self.totBatPower:
                    ChargeLastVal = ChargeLastVal + chargeCoeff
                else:
                    ChargeLastVal = self.totBatPower
            elif mode[i] == 1: # measure, solar panels not active and taking measurements
                ChargeLastVal = ChargeLastVal - measureCoeff
            else: # vidage station, solar panels not active + communication active
                ChargeLastVal = ChargeLastVal - vidageCoeff
            
            charge[i+1] = ChargeLastVal # here in Wh, later changed to %
        
        outputs['charge'] = charge/self.totBatPower*100 # from total charge to %
        outputs['mode'] = mode
    
    def setup_partials(self):
        # sparsity
        rovsJac = np.linspace(0,len(self.get_val('eclipses'))-1)
        colsJac = rovsJac #square Jacobian
        
        # Finite difference partials.
        self.declare_partials(of='charge', wrt=['eclipses'],
                              rows=rovsJac,
                              cols=colsJac,
                              method='fd')
        self.declare_partials(of='charge', wrt=['gsVisibility'],
                              rows=rovsJac,
                              cols=colsJac,
                              method='fd')

#------------------------------------------------------------------------------------------------------------
class DataDownload(om.ExplicitComponent):
    """
    Data transmission discipline
    Hypothesis :
        - As long as the satellite is visible form a gs and above the minimal elevation
          the data transfer ocures without interruptions
    
    Inputs :
        - gsVisibility (array) : binary array representing GS visibility at each timestep. {0-False, 1-True}
    Outputs :
        - downlinkRate (array) : time evolution of the data transfer speed, [bit/s]
        - downlinkCumulative (array) : time evolution of the data amount transfered, [Mbit]
        - downlinkTotal : total amount of data downloaded over the simulated period, [Mbit]
    """
    def initialize(self):
        # set constants in the problem
        # load input file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        
        # load datarates
        self.data_rate_down = float(self.config['DOWNLINK']['data_rate']) # bit/s
        # time step
        self.dt = float(self.config['DATA_BUDGET']['dt']) # s
    
    def setup(self):
        # input variables
        self.add_input('gsVisibility', shape_by_conn=True) # dynamic shape based on the size of external input
        
        # output variables
        self.add_output('downlinkRate', copy_shape='gsVisibility') # bit/s
        self.add_output('downlinkCumulative', copy_shape='gsVisibility') # Mbit
        self.add_output('downlinkTotal', val=0) # Mbit
    
    def compute(self, inputs, outputs):
        gsVisibility = inputs['gsVisibility'] # # quasi-boolean, {0,1}
        # Download
        self.downlinkRate = np.zeros(len(gsVisibility)) # bit/s, dynamic shape
        self.downlinkCumulative = np.zeros(len(gsVisibility)) # Mbit
        downlinkCumulativeLatest = 0 # Mbit
        
        # option A - discrete
        # for i in range(len(gsVisibility)):
        #     if gsVisibility[i] == 1: # i.e. True
        #         self.downlinkRate[i] = self.data_rate_down
        #         downlinkCumulativeLatest += self.data_rate_down / 1e6 * self.dt # 1e6 form bit to Mbit
        #     else:
        #         pass # if GS not wisible -> keep downlinkRate 0 and not add to cumulative
        #     self.downlinkCumulative[i] = downlinkCumulativeLatest
            
        # option B - alternatively, to have a differentiable function
        for i in range(len(gsVisibility)):
            self.downlinkRate[i] = self.data_rate_down * gsVisibility[i] # gsVisibility = 0 or 1
            downlinkCumulativeLatest += self.data_rate_down / 1e6 * self.dt * gsVisibility[i]# 1e6 form bit to Mbit
            self.downlinkCumulative[i] = downlinkCumulativeLatest
        outputs['downlinkRate'] = self.downlinkRate # bit/s
        outputs['downlinkCumulative'] = self.downlinkCumulative # Mbit (more for information)
        outputs['downlinkTotal'] = downlinkCumulativeLatest # Mbit (for goal)
        
        
    def setup_partials(self):
        # Analytical partials
        self.declare_partials('downlinkTotal', 'gsVisibility')
    
    def compute_partials(self, inputs, partials):
        # since total data exchange is basically an integral of the dataspeed*visibility, we can formally decklare partials as follows
        gsVisibility = inputs['gsVisibility'] # an array
        for i in range(len(gsVisibility)):
            partials['downlinkTotal','gsVisibility'][0][i] = self.data_rate_down / 1e6 * self.dt
        
#------------------------------------------------------------------------------------------------------------
class DataUpload(om.ExplicitComponent):
    """
    Data transmission discipline
    Hypothesis :
        - As long as the satellite is visible form a gs and above the minimal elevation
          the data transfer ocures without interruptions
    
    Inputs :
        - gsVisibility (array) : binary array representing GS visibility at each timestep. {0-False, 1-True}
    Outputs :
        - uplinkRate (array) : time evolution of the data transfer speed, [bit/s]
        - uplinkCumulative (array) : time evolution of the data amount transfered, [Mbit]
        - uplinkTotal : total amount of data downloaded over the simulated period, [Mbit]
    """
    def initialize(self):
        # set constants in the problem
        # load input file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        
        # load datarates
        self.data_rate_up = float(self.config['UPLINK']['data_rate']) # bit/s
        # time step
        self.dt = float(self.config['DATA_BUDGET']['dt']) # s
    
    def setup(self):
        # input variables
        self.add_input('gsVisibility', shape_by_conn=True) # dynamic shape based on the size of external input
        
        # output variables
        self.add_output('uplinkRate', copy_shape='gsVisibility') # bit/s, dynamic shape
        self.add_output('uplinkCumulative', copy_shape='gsVisibility') # Mbit
        self.add_output('uplinkTotal', val=0) # Mbit
    
    def compute(self, inputs, outputs):
        gsVisibility = inputs['gsVisibility'] # quasi-boolean, {0,1}
        # Download
        self.uplinkRate = np.zeros(len(gsVisibility)) # bit/s
        self.uplinkCumulative = np.zeros(len(gsVisibility)) # Mbit
        uplinkCumulativeLatest = 0 # Mbit
        
        # option A - discrete
        # for i in range(len(gsVisibility)):
        #     if gsVisibility[i] == 1: # i.e. True
        #         self.uplinkRate[i] = self.data_rate_up
        #         uplinkCumulativeLatest += self.data_rate_up / 1e6 * self.dt # 1e6 form bit to Mbit
        #     else:
        #         pass # if GS not wisible -> keep downlinkRate 0 and not add to cumulative
        #     self.uplinkCumulative[i] = uplinkCumulativeLatest
        
        # option B - alternatively, to have a differentiable function
        for i in range(len(gsVisibility)):
            self.uplinkRate[i] = self.data_rate_up * gsVisibility[i] # gsVisibility = 0 or 1
            uplinkCumulativeLatest += self.data_rate_up / 1e6 * self.dt * gsVisibility[i]# 1e6 form bit to Mbit
            self.uplinkCumulative[i] = uplinkCumulativeLatest
        
        outputs['uplinkRate'] = self.uplinkRate # bit/s (more for information)
        outputs['uplinkCumulative'] = self.uplinkCumulative # Mbit (more for information)
        outputs['uplinkTotal'] = uplinkCumulativeLatest # Mbit (for goal)
        
        
    def setup_partials(self):
        # Analytical partials
        self.declare_partials('uplinkTotal', 'gsVisibility')
        
        
    def compute_partials(self, inputs, partials):
        # since total data exchange is basically an integral of the dataspeed*visibility, we can formally decklare partials as follows
        gsVisibility = inputs['gsVisibility'] # an array
        for i in range(len(gsVisibility)):
            partials['uplinkTotal','gsVisibility'][0][i] = self.data_rate_up / 1e6 * self.dt

#------------------------------------------------------------------------------------------------------------
class Link(om.ExplicitComponent):
    """
    Link budget verification, Returns Eb/No for the chosen telecommunication scheme
    Hypothesis :
        - The worst-case scenario is examined - maximal possible distance between the Satellite and a GS with the worst combination of meteorogical conditions 
    
    Inputs :        
        - altitudeMax (float): Maximal altitude of the satellite on its orbit [km]
    Constants :
        - k_dB (float): Boltzmans constant [dB(J/K)]
        - c (float): Speed of light [m/s]
        - r_e (float): Earth radius [km]
        - elev_min (float): GS min elevatopn angle to communicate [deg]
        - f_d (float): Downlink frequency [Hz]
        - f_u (float): Upwnlink frequency [Hz]
        - P_sc_tx_dB (float): Satellite transmitter power [dBW] (converted form W in the input file)
        - G_sc_tx_dB (float): Satellite antenna gain [dB]
        - P_gs_tx_dB (float): Ground station transmitter power [dBW] (converted form W in the input file)
        - G_gs_tx_dB (float): Ground station antenna gain [dB]
        - data_rate_down (float): Downlink datarate [bit/s]
        - data_rate_up (float): Uplink datarate [bit/s]
        - marginDownloadRaw (float): C/N0 raw margin (calculated at execution) [dB]
        - marginUploadRaw (float): C/N0 raw margin (calculated at execution) [dB]
        - T_gs_rx_dB (float): GS receiver equialent temperature [dBK] (converted form K in the input file)
        - T_sc_rx_dB  (float): Satellite receiver equialent temperature [dBK] (converted form K in the input file)
        - d_gs (float): GS dish diameter [m]
        - eta_gs_rx (float): GS rx efficiency, 
        - G_gs_rx_dB (float): GS receiver gain (calculated at setup) [dB]
        - G_sc_rx_dB (float): Satellite receiver gain [dB]
        - L_sc_tx_dB (float): Satellite transmitter losses (calculated at setup from all the partial losses) [dB]
        - L_gs_tx_dB (float): GS transmitter losses (calculated at setup from all the partial losses) [dB]
        - L_atm_dB (float): Aditional atmospheric losses, same for uplink and downlink - worst case (calculated at setup from all the partial losses), [dB]
        - L_gs_rx_dB (float): Ground station receiver losses [dB]
        - L_sc_rx_dB (float): Spacecraft receiver losses [dB]
    Outputs :
        - float: marginUpload, uplink Eb/No in the worst-case scenario [dB]
        - float: marginDownload, downlink Eb/No in the worst-case scenario [dB]
    """
    
    def initialize(self):
        """Load constants from imput.yaml
        """
        
        """
        # --------------Option A--------------
        #Using OpenMDAO formalism
        # natural constants
        self.options.declare('k_dB', default=0, desc='Boltzmans constant, dB(J/K)')
        self.options.declare('c', default=0, desc='Speed of light, m/s')
        self.options.declare('r_e', default=0, desc='Earth radius, km')
        
        # satellite transmitter
        self.options.declare('f_d', default=0, desc='Downlink frequency, Hz')
        self.options.declare('lambda_d', default=0, desc='Downlink wavelength, Hz')
        self.options.declare('P_tx_dB', default=0, desc='Satellite transmitter power, dBm')
        self.options.declare('G_sc_tx', default=0, desc='Satellite antenna gain, dB')
        
        # GS receiver
        self.options.declare('T_rx_dB', default=0, desc='GS receiver equialent temperature, dB(K)')
        self.options.declare('G_gs_dB', default=0, desc='GS receiver gain, dB')
        
        # ------losses------
        # satellite tx losses
        self.options.declare('L_sc_tx', default=0, desc='Satellite transmitter losses, dB')
        # path losses
        self.options.declare('L_fsl', default=0, desc='Free space losses, dB')
        self.options.declare('L_atm', default=0, desc='Aditional atmospheric losses, dB')
        # GS rx losses
        self.options.declare('L_gs_rx', default=0, desc='Ground station receiver losses, dB')
        """
        
        # ----------------Option B------------------------------
        # set constants in the problem
        # load input file into config variable 
        configFileName = "./data/input.yaml" # input file, can be changed later to an execution parameter
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)
        
        # get natural constants values
        self.k_dB = float(self.config['CONSTANTS']['k_dB']) # Boltzmans constant, dB(J/K) 
        self.c = float(self.config['CONSTANTS']['c']) # Speed of light, m/s
        self.r_e = float(self.config['CONSTANTS']['radius_earth']) # Earth radius, km
        
        # satellite transmitter
        self.f_d = float(self.config['DOWNLINK']['frequency']) # Downlink frequency, Hz
        self.lambda_d = self.c/self.f_d # Downlink wavelength, m
        self.P_sc_tx_dB = to_db(float(self.config['SPACECRAFT_TRANSMITTER']['SC_power_tx'])) # Satellite transmitter power, dBW # TODO - Next phase - set as design parameter, creating a coupling with power discipline
        self.G_sc_tx_dB = float(self.config['SPACECRAFT_TRANSMITTER']['SC_ant_gain_tx']) # Satellite antenna gain, dB
        # GS transmitter
        self.f_u = float(self.config['UPLINK']['frequency']) # Upwnlink frequency, Hz
        self.lambda_u = self.c/self.f_u # Upwnlink wavelength, m
        self.P_gs_tx_dB = to_db(float(self.config['GROUND_STATION_TRANSMITTER']['GS_power_tx'])) # Ground station transmitter power, dBW # TODO - Next phase - set as design parameter, creating a coupling with power discipline
        self.G_gs_tx_dB = float(self.config['GROUND_STATION_TRANSMITTER']['GS_ant_gain_tx']) # Ground station antenna gain, dB
        
        #data flow
        self.data_rate_down = float(self.config['DOWNLINK']['data_rate']) # Downlink datarate, bps 
        self.marginDownloadRaw = 0.0 # C/N0 raw margin, dB; to be calculated
        self.data_rate_up = float(self.config['UPLINK']['data_rate']) # Downlink datarate, bps 
        self.marginUploadRaw = 0.0 # C/N0 raw margin, dB; to be calculated
        
        # visibility limits
        self.elev_min = float(self.config['GROUNDSTATION_LOCATION']['GS_minElevation']) # GS min elevatopn angle to communicate, deg
        self.elev_min_rad = np.deg2rad(self.elev_min) # GS min elevatopn angle to communicate, rad
        
        # GS receiver
        self.T_gs_rx_dB = to_db(float(self.config['GROUND_STATION_RECEIVER']['GS_T_rx'])) # GS receiver equialent temperature, dBK
        self.d_gs = float(self.config['GROUND_STATION_RECEIVER']['antennaDiameter']) # GS dish diameter, m
        self.eta_gs_rx = float(self.config['GROUND_STATION_RECEIVER']['eta_rx']) # GS rx efficiency, 
        self.G_gs_rx_dB = to_db(self.eta_gs_rx * (np.pi*self.d_gs / self.lambda_d)**2) # GS receiver gain, dB
        # Satellite receiver
        self.T_sc_rx_dB = to_db(float(self.config['SPACECRAFT_RECEIVER']['SC_T_rx'])) # Satellite receiver equialent temperature, dBK
        self.G_sc_rx_dB = float(self.config['SPACECRAFT_RECEIVER']['SC_ant_gain_rx']) # Satellite receiver gain, dB
        
        # ------losses------
        # satellite tx losses
        self.L_sc_tx_dB = (float(self.config['SPACECRAFT_TRANSMITTER']['SC_misc'])
                        + float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_cable_tx'])
                        + float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_connector_tx'])
                        + float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_feeder_tx'])
                        + float(self.config['SPACECRAFT_TRANSMITTER']['SC_loss_point_tx'])
                        ) # Satellite transmitter losses, dB
        # GS tx losses
        self.L_gs_tx_dB = (float(self.config['GROUND_STATION_TRANSMITTER']['GS_line_loss_tx'])   
                        +  float(self.config['GROUND_STATION_TRANSMITTER']['GS_loss_connector_tx'])   
                        +  float(self.config['GROUND_STATION_TRANSMITTER']['GS_loss_point_rx']) 
                        ) # GS transmitter losses, dB
        # path losses
        self.L_fsl_down_dB = 0 # initialization
        self.L_fsl_up_dB = 0 # initialization
        # self.L_fsl_dB = f(altitudeMax)# Free space losses, dB depend on altitude_max
        self.L_atm_dB = (float(self.config['PROPAGATION_LOSSES']['loss_pol'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_atm'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_scin'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_rain'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_cloud'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_si'])
                      + float(self.config['PROPAGATION_LOSSES']['loss_misc'])
                      ) # Aditional atmospheric losses, same for uplink and downlink - worst case, dB
        # GS rx losses 
        self.L_gs_rx_dB = float(self.config['GROUND_STATION_RECEIVER']['GS_loss_point_rx']) # Ground station receiver losses, dB
        # Satellite rx losses
        self.L_sc_rx_dB = float(self.config['SPACECRAFT_RECEIVER']['SC_loss_point_rx']) # Spacecraft receiver losses, dB
    
    def setup(self):
        #input variables
        self.add_input('altitudeMax', val=600) # km
        
        #output variables
        self.add_output('marginUpload', val=0) # Eb/N0[dB]
        self.add_output('marginDownload', val=0) # [dB]
        
    def compute(self, inputs, outputs):
        
        # calculate the free space losses
        r = self.r_e + inputs['altitudeMax'] # km
        d = self.r_e*((r**2 / self.r_e**2 - (np.cos(self.elev_min_rad))**2)**0.5 - np.sin(self.elev_min_rad)) # distance at the limit of visibility, km
        self.L_fsl_down_dB = to_db((4.0*np.pi * d*1000.0 / self.lambda_d)**2.0) # note - d from km to m
        self.L_fsl_up_dB = to_db((4.0*np.pi * d*1000.0 / self.lambda_u)**2.0) # note - d from km to m
        
        self.L_down_dB = self.L_sc_tx_dB + self.L_fsl_down_dB + self.L_atm_dB + self.L_gs_rx_dB # downlink path losses, dB
        self.L_up_dB = self.L_gs_tx_dB + self.L_atm_dB + self.L_fsl_up_dB + self.L_sc_rx_dB # uplink path losses, dB
        # C/N0 margins
        self.marginDownloadRaw = self.P_sc_tx_dB + self.G_sc_tx_dB + self.G_gs_rx_dB - self.L_down_dB - self.k_dB - self.T_gs_rx_dB
        self.marginUploadRaw = self.P_gs_tx_dB + self.G_gs_tx_dB + self.G_sc_rx_dB - self.L_up_dB - self.k_dB - self.T_sc_rx_dB
        
        outputs['marginDownload']= self.marginDownloadRaw - to_db(self.data_rate_down)
        
        outputs['marginUpload']= self.marginUploadRaw - to_db(self.data_rate_up)
        
    
    def setup_partials(self):
        # Finite difference partials.
        self.declare_partials('marginUpload', 'altitudeMax', method='fd')
        self.declare_partials('marginDownload', 'altitudeMax', method='fd')


# ------------------------- Disciplines groups --------------------------------------------------------------
# for better organizations some disciplines are devided into smaller parts and consolidated in groups

class OrbitalDynamics(om.Group):
    """Group containing Orbital dynamics disciplines
    Disciplines included:
        - OrbitalDynamicsVisibility
        - OrbitalDynamicsAltitude
    """
    def setup(self):
        self.add_subsystem('OrbitalDynamicsVisibility', OrbitalDynamicsVisibility())
        self.add_subsystem('OrbitalDynamicsAltitude', OrbitalDynamicsAltitude())
        
        # self.nonlinear_solver = om.NewtonSolver(solve_subsystems=False) # universal
        self.linear_solver = om.LinearRunOnce() #uncoupled subsystem
    def configure(self):
        """Group configuration
        """
        self.promotes('OrbitalDynamicsVisibility',inputs=['a', 'exc', 'inc', 'raan', 'aop', 'ta', 't0'])
        self.promotes('OrbitalDynamicsAltitude',inputs=['a', 'exc'])
    
#------------------------------------------------------------------------------------------------------------
class Data(om.Group):
    def setup(self):
        self.add_subsystem('DataDownload', DataDownload())
        self.add_subsystem('DataUpload', DataUpload())
        
        self.linear_solver = om.LinearRunOnce() #uncoupled subsystem
    def configure(self):
        """Group configuration
        """
        self.promotes('DataDownload',inputs=['gsVisibility'])
        self.promotes('DataUpload',inputs=['gsVisibility'])
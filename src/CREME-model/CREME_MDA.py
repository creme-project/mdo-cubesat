# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

MDA part of the CREME optimization problem
"""

from matplotlib import pyplot as plt
import openmdao.api as om
from omxdsm import write_xdsm
from Disciplines import *

class CREME_MDA(om.Group):
    """
    MDA part of the CREME optimization problem as an instance of openmdao.api.Group
    """
    
    def setup(self):        
        self.add_subsystem('OrbitDynamics', OrbitalDynamics())
        self.add_subsystem('Power', Power())
        self.add_subsystem('Data', Data()) # a Group child
        self.link = self.add_subsystem('Link', Link()) 
        
        #internal connections
        self.connect('OrbitDynamics.eclipses', 'Power.eclipses')
        self.connect('OrbitDynamics.gsVisibility', 'Power.gsVisibility')
        self.connect('OrbitDynamics.gsVisibility', 'Data.gsVisibility')
        self.connect('OrbitDynamics.altitudeMax', 'Link.altitudeMax')
        
        # Solvers - here not necessary
        #self.nonlinear_solver = om.NewtonSolver(solve_subsystems=False)
        #self.linear_solver = om.DirectSolver()
    def configure(self):
        """Group configuration after setup
        """
        #self.promotes('Link',inputs=['altitudeMax'],outputs=['marginUpload'])
        #self.promotes('Data',inputs=['gsVisibility'])
        #self.promotes('Link',outputs=['marginUpload', 'marginDownload'])
        #self.promotes('Data',outputs=['*'])
        #self.promotes('Power',outputs=['*'])

# --------------debugging-------------------

if __name__ == "__main__":
    prob_CREME = om.Problem()
    prob_CREME.model = CREME_MDA()
    # seting up the problem
    prob_CREME.setup()
    
    prob_CREME.run_model()
    # Check link
    marginDownload = prob_CREME.get_val('Link.marginDownload')
    print(f"Margin Downlink, dB:{marginDownload}")
    marginUpload = prob_CREME.get_val('Link.marginUpload')
    print(f"Margin Uplink, dB:{marginUpload}")
    
    # Check Power
    charge = prob_CREME.get_val('Power.charge')
    mode = prob_CREME.get_val('Power.mode')
    print("Power discipline")
    print(f"Battery charge state, Wh:{charge}")
    print(f"Operation mode: {mode}")
    # Check Data
    downlinkTotal = prob_CREME.get_val('Data.downlinkTotal')
    downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
    downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
    print("Data discipline")
    print(f"Total Downloaded, Mbit:\t{downlinkTotal}")
    print(f"Download rate, bit/s:\t{downlinkRate}")
    print(f"Download over time, Mbit:{downlinkCumulative}")
    
    # generate N2
    om.n2(prob_CREME, 'N2_diagrams/N2-CREME_MDA_testing')
    # generate XDSM
    # Write output. PDF will only be created, if pdflatex is installed
    # Error to be resolved - output of designVars* from each element of the group - that shouldn't be happening
    write_xdsm(prob_CREME, filename='XDSM_diagrams/CREME_MDA_XDSM_pyxdsm', out_format='html', show_browser=True,
                quiet=False, output_side='left', include_indepvarcomps=True)
    
    # plotting
    time = np.arange(0, len(prob_CREME.model.get_val('OrbitDynamics.eclipses')), 1)
    fig, axs = plt.subplots(3)
    axs[0].plot(time, prob_CREME.get_val('OrbitDynamics.eclipses'), '#8888FF')
    axs[0].plot(time, prob_CREME.get_val('OrbitDynamics.gsVisibility'), 'b')
    axs[0].set_title("Eclipses and Visibility")
    axs[1].plot(time, prob_CREME.get_val('Data.DataDownload.downlinkCumulative'), 'k')
    ax2=axs[1].twinx()    
    ax2.plot(time, prob_CREME.get_val('Data.DataDownload.downlinkRate'), 'b')
    axs[1].set_title("Download")
    axs[2].plot(time, prob_CREME.get_val('Power.charge'))    
    
    plt.show()
"""
A debugging code open for experimentation
consists of 2 parts - validation of functions used in OrbitalDynamics
                    - Validation of the implementation inside Disciplines.py and DisciplinesAnalyticPartials.py
"""

from ast import If
import openmdao.api as om

import numpy as np
import yaml
import yamlloader
from Disciplines import OrbitalDynamics
import os

from functionsDatabase import delUnits, to_db, writeGMATscript, detectEvents, parseEvents

from datetime import datetime

# derivatives check function
from openmdao.test_suite.test_examples.test_betz_limit import ActuatorDisc

#============== Separate Functions Testing==============================================
#  # load input.yaml file into config variable 
# configFileName = "./data/input.yaml" # input file, can be changed later
# stream = open(configFileName, 'r')
# config = yaml.load(stream, Loader = yamlloader.ordereddict.CSafeLoader)
# stream.close()
# config = delUnits(config)

# a = config['ORBIT']['SMA']
# exc = config['ORBIT']['EXC']
# inc = config['ORBIT']['INC']
# raan = config['ORBIT']['RAAN']
# aop = config['ORBIT']['AOP']
# ta = config['ORBIT']['TA']
# t0 = datetime.strptime(config['ORBIT']['epoch'], '%Y,%m,%d,%H,%M,%S').timestamp() # get UNIX timestamp from input.yaml's YYYY,MM,DD,hh,mm,ss

# # load constants
# tTotal = config['SIMULATION']['total_time'] * 86400 # mean synodic day to sec
# dt = config['DATA_BUDGET']['dt'] # Important! take dt from DATA_BUDGET bacause it is has a different value from dt in SIMULATION
# t0 = datetime(2023, 1, 1, 0, 0, 0).timestamp()
# # load GMAT and its input and output files
# options = {}
# options['main_dir'] = "/home/dcas/pa.lamos/Documents/Stage\ DCAS/2022-pir-patrik_lamos/src/CREME-model/"
# GMAT_bin = config['paths']['GMAT_BIN_linux']
# GMAT_inputFile = options['main_dir'] + config['paths']['GMAT_input_dir'] + "MissionFromPython.script" # GMAT reads input form here

# # check GMAT input script generation
# writeGMATscript(config, a, exc, inc, raan, aop, ta, t0)

# # check GMAT path
# print("============== running GMAT ==================")
# print(f"{GMAT_bin} --minimize --run {GMAT_inputFile} --exit")

# os_res = os.system(f"{GMAT_bin} --minimize --run {GMAT_inputFile} --exit")
# if os_res != 0:
#         exit('ERROR : Verify the path to your GMAT application in the input file')


# eclipseData = detectEvents(config['paths']['GMAT_output_dir']+"EclipseLocator1.txt", t0)
# visibilityData = detectEvents(config['paths']['GMAT_output_dir']+"ContactLocator1.txt", t0)
# # change to per-dt array
# eclipses = parseEvents(eclipseData, tTotal, dt)
# gsVisibility = parseEvents(visibilityData, tTotal, dt)


# print(eclipseData)
# print(eclipses[2160])
# print(eclipses[2200])
# print(eclipses[4000])
# =========================Integrated discipline=============================================================

# prob_OrbitalDynamics = om.Problem()
# prob_OrbitalDynamics.model.add_subsystem('OrbitalDynamics',OrbitalDynamics(),
#                                          promotes_inputs=['a', 'exc', 'inc', 'raan', 'aop', 'ta', 't0'],
#                                          promotes_outputs=['eclipses','gsVisibility','altitudeMax'])
# #prob_OrbitalDynamics.model.nonlinear_solver = om.NewtonSolver(solve_subsystems=False)
# prob_OrbitalDynamics.model.linear_solver = om.DirectSolver()
# prob_OrbitalDynamics.setup()
# prob_OrbitalDynamics.set_val('OrbitalDynamics.t0', val=datetime(2023, 1, 1, 0,0,0).timestamp())


# prob_OrbitalDynamics.run_model()

# prob_OrbitalDynamics.model.list_inputs()
# prob_OrbitalDynamics.model.list_outputs()

# print(prob_OrbitalDynamics.model.get_val('eclipses')[1440:1445]) # here should be a passage from not-eclipsed to eclipsed
# ======================Discipline using analytical derivatives====================================
# -----------------A-------------------
import DisciplinesAnalyticPartials as DisciplinesAP

# probAltitude = om.Problem()
# probAltitude.model.add_subsystem('OrbitalDynamicsAltitude', DisciplinesAP.OrbitalDynamicsAltitude())

# probAltitude.model.linear_solver = om.DirectSolver()

# probAltitude.setup()
# probAltitude.set_val('OrbitalDynamicsAltitude.a', val=6978.0)
# probAltitude.set_val('OrbitalDynamicsAltitude.exc', val=0.1)

# probAltitude.check_partials(compact_print=True)
# probAltitude.run_model()

# probAltitude.model.list_outputs()
# -----------------B-------------------

prob_OrbitalDynamics = om.Problem()
prob_OrbitalDynamics.model.add_subsystem('OrbitalDynamics', DisciplinesAP.OrbitalDynamics())

prob_OrbitalDynamics.model.linear_solver = om.DirectSolver()

prob_OrbitalDynamics.setup()

prob_OrbitalDynamics.set_val('OrbitalDynamics.t0', val=datetime(2023, 1, 1, 0,0,0).timestamp())
#prob_OrbitalDynamics.set_val('OrbitalDynamics.a', val=10000)
#prob_OrbitalDynamics.set_val('OrbitalDynamics.exc', val=0.1)


prob_OrbitalDynamics.run_model()

prob_OrbitalDynamics.model.list_inputs()
prob_OrbitalDynamics.model.list_outputs()

print(prob_OrbitalDynamics.model.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')[1440:1445]) # here should be a passage from not-eclipsed to eclipsed

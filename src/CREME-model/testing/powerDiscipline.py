"""Development of the power discipline, which caused a lot of headache.
Left in the project as some further inquires might be necessary since there are still troubles with SLSQP optimization algorithm

"""

import numpy as np
import matplotlib.pyplot as plt

def computeBatteryConsumption(dt, eclipseData, contactData, totBatPower, solarPower, Pmesure, Pvidage):

    """Compute Battery DOD over the simulation period.

    Hypothesis: 
       -  3 modes are considered for the nanosat: Charge, Mesure (nominal), Vidage
       -  All eclipses are considered as dark (umbra)
       -  During charging, the satellite continues to mesure (the power for mode Mesure is needed during charging)

    Args: 
        dt (float): time step [sec]
        eclipseData (list): List of eclipse state every time step, Boolean
        contactData (list): List of ground station visibility every time step, Boolean
        totBatPower (float): Total battery energy [Wh]
        solarPower (float): Power given by the solar panels [W]
        PMesure (float): Power consumed during the mesure mode [W]
        Pvidage (float): Power consumed during the vidage mode [W]

    Returns:
        list: list containing:

            int array: mode, describe satellite mode at each second (0: Charge; 1 : Mesure; 2 : Vidage station) - just for information and plotting

            float array: remaining_power, Remaining power in the battery at each second during simulation [Wh] - constrained by DOD_max
    """

    

    mode = np.zeros(len(eclipseData)) # 0: Charge 1 : Mesure 2 : Vidage station

    # Deside the mode of operation, eclipses first, GS contact on top of them
    for i in range(len(eclipseData)):
        if eclipseData[i] == True:
            mode[i] = 1

    # GS contact
    for i in range(len(contactData)):
        if contactData[i] == True:
            mode[i] = 2
            
    #compute remaining power
    remaining_power = [totBatPower]
    lastVal = totBatPower
    vidageCoeff = Pvidage/3600.0*dt # to get from W to Wh by the fact that timestep unit here is in seconds
    mesureCoeff = Pmesure/3600.0*dt
    chargeCoeff = (solarPower - Pmesure)/3600.0*dt
    for u in range(len(mode)-1):
        if mode[u] == 0:
            if lastVal < totBatPower:
                lastVal = lastVal + chargeCoeff
            else:
                lastVal = totBatPower
        elif mode[u] == 1:
            lastVal = lastVal - mesureCoeff
        else:
            lastVal = lastVal - vidageCoeff
        
        remaining_power.append(lastVal)
    
    return [mode, remaining_power]

# ------------ debugging--------------
if __name__ == "__main__":
    dt = 1 # s
    t = range(50)
    eclipseData = [False]*50
    eclipseData[10:40]=[True]*30
    contactData = [False]*50
    contactData[20:30]=[True]*10
    totBattPower = 80 # Wh
    solarPower = 24 # W
    Pmesure = 9 # W
    Pvidage = 18 # W
    
    [mode, energy] = computeBatteryConsumption(dt, eclipseData, contactData, totBattPower, solarPower, Pmesure, Pvidage)
    
    print(mode)
    print(energy)
    print(len(energy))
    
    fig, axs = plt.subplots(3, 1, sharex=True, sharey=False)
    axs[0].plot(t, energy)
    axs[0].set_title("Battery charge")
    axs[1].plot(t, mode)
    axs[1].set_title("Mode")
    axs[2].plot(t, eclipseData, '-r')
    axs[2].plot(t, contactData)
    axs[2].set_title("Visibilities")
    
    plt.show()
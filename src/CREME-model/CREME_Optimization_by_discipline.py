# -*- coding: utf-8 -*-
"""
@author Patrik Lamos

Debug focused script, optimization discipline-by-discipline to validate formulations in Disciplines
To run each part, uncomment it inside if __name__ == '__main__': ...
"""

import openmdao.api as om
from DisciplinesAnalyticPartials import *
from functionsDatabase import plotEvolutions, plotConvergence
#------------------------------------------------------------------------------------------------------------
def problemLinkImplementation():
    """Maximize downlink margin by changing max altitude. Constrained by Upload margin
    - behaves as expected
    """
    prob_Link = om.Problem()
    prob_Link.model.add_subsystem('Link', Link())
    # set up optimizer
    prob_Link.driver = om.ScipyOptimizeDriver()
    prob_Link.driver.options['optimizer'] = 'SLSQP'
    prob_Link.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_Link.model.add_design_var('Link.altitudeMax', lower=450, upper=2000) # km
    prob_Link.model.add_objective('Link.marginDownload',scaler=-1.0) # -1.0-> maximize dB in the actual implementation it will serve as a constraint
    prob_Link.model.add_constraint('Link.marginUpload', 9.6, 50) # dB in the actual implementation it will serve as a constraint

    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    # seting up the problem
    prob_Link.setup()

    #check initialization
    print("After initialization")
    print(f"Link.altitudeMax = {prob_Link.get_val('Link.altitudeMax')}")
    
    prob_Link.run_model()
    # check after single solver run
    print("After initial solver run")
    prob_Link.list_problem_vars()

    # execute optimization
    prob_Link.run_driver()
    # get results
    print("After optimization run")
    prob_Link.list_problem_vars()

    # generate N2
    om.n2(prob_Link, "N2_diagrams/separate_disciplines/N2-LinkDiscipline.html")
#------------------------------------------------------------------------------------------------------------
def problemDataImplementation():
    """Maximize total download by changing gs visibility. Constrained by total upload
    - behaves as expected
    """
    # initialize variable to define shape
    gsVisibility = np.zeros(100)
    gsVisibility[40:60] = np.ones(20)
    prob_Data = om.Problem()
    
    prob_Data.model.add_subsystem('gsVisibilityInput', om.IndepVarComp('gsVisibility', val=gsVisibility))
    prob_Data.model.add_subsystem('Data', Data())
    prob_Data.model.connect('gsVisibilityInput.gsVisibility','Data.gsVisibility')
    
    # set up optimizer
    prob_Data.driver = om.ScipyOptimizeDriver()
    prob_Data.driver.options['optimizer'] = 'SLSQP'
    prob_Data.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_Data.model.add_design_var('Data.gsVisibility', lower=0, upper=1) # contact or not
    prob_Data.model.add_objective('Data.downlinkTotal',scaler=-1.0) # -1.0-> maximize Mbit in the actual implementation it will serve as an objective
    prob_Data.model.add_constraint('Data.uplinkTotal', 0) # Mbit in the actual implementation it will serve to validate 

    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    # seting up the problem
    prob_Data.setup()

    #check initialization
    print("After initialization")
    print(f"Data.gsVisibility = {prob_Data.get_val('Data.gsVisibility')}")
    
    prob_Data.run_model()
    # check after single solver run
    print("After initial solver run")
    prob_Data.list_problem_vars()

    # execute optimization
    prob_Data.run_driver()
    # get results
    print("After optimization run")
    prob_Data.list_problem_vars()
    
    print(f"Data.gsVisibility = {prob_Data.get_val('Data.gsVisibility')}")

    # generate N2
    # om.n2(prob_Data, "N2_diagrams/separate_disciplines/N2-DataDiscipline.html")
    
#------------------------------------------------------------------------------------------------------------
def problemPowerImplementation():
    """Doesn't work, unless a new scalar variable is introduced as an objective
    """
    # initialize variable to define shape
    gsVisibility = np.zeros(100)
    gsVisibility[40:60] = np.ones(20)
    eclipses = np.zeros(100)
    eclipses[20:50] = np.ones(30)
    
    prob_Power = om.Problem()
    
    prob_Power.model.add_subsystem('gsVisibilityInput', om.IndepVarComp('gsVisibility', val=gsVisibility))
    prob_Power.model.add_subsystem('eclipsesInput', om.IndepVarComp('eclipses', val=eclipses))
    prob_Power.model.add_subsystem('Power', Power())
    prob_Power.model.connect('gsVisibilityInput.gsVisibility','Power.gsVisibility')
    prob_Power.model.connect('eclipsesInput.eclipses','Power.eclipses')
    
    # set up optimizer
    prob_Power.driver = om.ScipyOptimizeDriver()
    prob_Power.driver.options['optimizer'] = 'SLSQP'
    prob_Power.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_Power.model.add_design_var('Power.gsVisibility', lower=0, upper=1) # contact or not
    prob_Power.model.add_design_var('Power.eclipses', lower=0, upper=1) # contact or not
    # ----------- in the future, try implementing a scalar variable to make a check-run of Power
    prob_Power.model.add_objective('Power.TotalCharge',scaler=-1.0) # -1.0-> maximize Mbit in the actual implementation it will serve as an objective
    prob_Power.model.add_constraint('Power.mode', 1) # Mbit in the actual implementation it will serve to validate 

    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    # seting up the problem
    prob_Power.setup()

    #check initialization
    print("After initialization")
    print(f"Data.gsVisibility = {prob_Power.get_val('Power.gsVisibility')}")
    print(f"Data.gsVisibility = {prob_Power.get_val('Power.eclipses')}")
    
    prob_Power.run_model()
    # check after single solver run
    print("After initial solver run")
    prob_Power.list_problem_vars()

    # execute optimization
    prob_Power.run_driver()
    # get results
    print("After optimization run")
    prob_Power.list_problem_vars()

    # generate N2
    # om.n2(prob_Data, "N2_diagrams/separate_disciplines/N2-DataDiscipline.html")
#------------------------------------------------------------------------------------------------------------
def problemOrbitAndLink():
    """Maximize downlink margin by changing orbital parameters. Constrained by Upload margin
    - 
    """
    prob_OrbitLink = om.Problem()
    prob_OrbitLink.model.add_subsystem('OrbitalDynamicsAltitude', OrbitalDynamicsAltitude())
    prob_OrbitLink.model.add_subsystem('Link', Link())
    # set up optimizer
    prob_OrbitLink.driver = om.ScipyOptimizeDriver()
    prob_OrbitLink.driver.options['optimizer'] = 'SLSQP'
    prob_OrbitLink.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_OrbitLink.model.add_design_var('OrbitalDynamicsAltitude.a', lower=6778, upper=8500) # km
    prob_OrbitLink.model.add_design_var('OrbitalDynamicsAltitude.exc', lower=0.0, upper=0.1) # km
    
    prob_OrbitLink.model.add_objective('Link.marginDownload',scaler=-1.0) # -1.0-> maximize dB in the actual implementation it will serve as a constraint
    prob_OrbitLink.model.add_constraint('OrbitalDynamicsAltitude.altitudeMin', 450) # km 

    prob_OrbitLink.model.connect('OrbitalDynamicsAltitude.altitudeMax','Link.altitudeMax')
    # prob_OrbitLink.set_solver_print(level=0) # print msg only if the solver fails
    # prob_OrbitLink.model.approx_totals() # not necessary
    # seting up the problem
    prob_OrbitLink.setup()

    #check initialization
    print("After initialization")
    print(f"Link.altitudeMax = {prob_OrbitLink.get_val('Link.altitudeMax')}")
    
    prob_OrbitLink.run_model()
    # check after single solver run
    print("After initial solver run")
    prob_OrbitLink.list_problem_vars()

    # execute optimization
    prob_OrbitLink.run_driver()
    # get results
    print("After optimization run")
    prob_OrbitLink.list_problem_vars()

    # generate N2
    om.n2(prob_OrbitLink, "N2_diagrams/separate_disciplines/N2-OrbitLinkDisciplines.html")
#------------------------------------------------------------------------------------------------------------
def problemOrbitAndData():
    """Maximize download by changing orbital parameters. Constrained by altitude and inclination
    - 
    """
    prob_OrbitData = om.Problem()
    prob_OrbitData.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
    prob_OrbitData.model.add_subsystem('Data', Data())
    # set up solver
    #prob_OrbitData.model.linear_solver = om.DirectSolver()
    # set up optimizer
    prob_OrbitData.driver = om.ScipyOptimizeDriver()
    prob_OrbitData.driver.options['optimizer'] = 'COBYLA' # COBYLA functions better than SQLSP
    prob_OrbitData.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_OrbitData.model.add_objective('Data.downlinkTotal',scaler=-1.0) # -1.0-> maximize, Mbit,
    # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1)
    prob_OrbitData.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=1000) # km
    #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
    prob_OrbitData.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
    
    prob_OrbitData.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=450, ref=100) # km 

    prob_OrbitData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    # seting up the problem
    prob_OrbitData.setup()

    #check initialization
    print("After initialization")
    print(f"a = {prob_OrbitData.get_val('OrbitalDynamics.a')}")
    
    prob_OrbitData.run_model()
    # check after single solver run
    print("After initial solver run")
    prob_OrbitData.list_problem_vars()

    # execute optimization
    prob_OrbitData.run_driver()
    # get results
    print("After optimization run")
    prob_OrbitData.list_problem_vars()

    # generate N2
    om.n2(prob_OrbitData, "N2_diagrams/separate_disciplines/N2-OrbitDataDisciplines.html")

#------------------------------------------------------------------------------------------------------------
def problemOrbitPowerData():
    """Maximize download by changing orbital parameters. Constrained by altitude, inclination and maximal depth of discharge
    """
    prob_OrbitPowerData = om.Problem()
    prob_OrbitPowerData.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
    prob_OrbitPowerData.model.add_subsystem('Data', Data())
    prob_OrbitPowerData.model.add_subsystem('Power', Power())
    
    # set up solver
    prob_OrbitPowerData.model.linear_solver = om.DirectSolver()
    # set up optimizer
    prob_OrbitPowerData.driver = om.ScipyOptimizeDriver()
    prob_OrbitPowerData.driver.options['optimizer'] = 'COBYLA'
    prob_OrbitPowerData.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_OrbitPowerData.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1.0) # -1.0-> maximize, Mbit,
    # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1)
    prob_OrbitPowerData.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=1000) # km
    #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
    prob_OrbitPowerData.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
    
    prob_OrbitPowerData.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=450, ref=100) # km
    prob_OrbitPowerData.model.add_constraint('Power.charge', lower=70, ref=10) # %

    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
    
    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    
    # seting up the problem
    prob_OrbitPowerData.setup()

    #check initialization
    print("After initialization")
    print(f"a = {prob_OrbitPowerData.get_val('OrbitalDynamics.a')}")
    
    prob_OrbitPowerData.run_model()
    # check after single solver run
    print("*******After initial solver run:*******")
    prob_OrbitPowerData.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max'])
    #display time evolutions
    eclipses = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkRate')
    charge = prob_OrbitPowerData.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    # execute optimization
    prob_OrbitPowerData.run_driver()
    # get results
    print("*******After optimization run:*******")
    prob_OrbitPowerData.list_problem_vars(driver_scaling=False, cons_opts=['min','max'])
    #display time evolutions
    eclipses = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkRate')
    charge = prob_OrbitPowerData.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")
    
    # generate N2
    om.n2(prob_OrbitPowerData, "N2_diagrams/separate_disciplines/N2-OrbitPowerDataDisciplines.html")
#------------------------------------------------------------------------------------------------------------
def problemOrbitPowerDataRecording():
    """Maximize download by changing orbital parameters. Constrained by altitude, inclination and maximal depth of discharge
    """
    prob_OrbitPowerData = om.Problem()
    prob_OrbitPowerData.model.add_subsystem('OrbitalDynamics', OrbitalDynamics())
    prob_OrbitPowerData.model.add_subsystem('Data', Data())
    prob_OrbitPowerData.model.add_subsystem('Power', Power())
    
    # set up solver
    prob_OrbitPowerData.model.linear_solver = om.DirectSolver()
    # set up optimizer
    prob_OrbitPowerData.driver = om.ScipyOptimizeDriver()
    prob_OrbitPowerData.driver.options['optimizer'] = 'COBYLA'
    prob_OrbitPowerData.driver.options['tol'] = 1e-8
    # set up optimization problem
    prob_OrbitPowerData.model.add_objective('Data.DataDownload.downlinkTotal',scaler=-1.0) # -1.0-> maximize, Mbit,
    # using scaling to try make the optimization run more smoothly, order of magnitude for design vars: O(1)
    prob_OrbitPowerData.model.add_design_var('OrbitalDynamics.a', lower=6778, upper=8500, ref=1000) # km
    #prob_OrbitData.model.add_design_var('OrbitalDynamics.exc', lower=0.0, upper=0.1, ref=0.1) # km - doesn't seem to have an effect
    prob_OrbitPowerData.model.add_design_var('OrbitalDynamics.inc', lower=80, upper=110, ref=10) # deg
    
    prob_OrbitPowerData.model.add_constraint('OrbitalDynamics.OrbitalDynamicsAltitude.altitudeMin', lower=450, ref=100) # km
    prob_OrbitPowerData.model.add_constraint('Power.charge', lower=70, ref=10) # %

    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Data.gsVisibility')
    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility','Power.gsVisibility')
    prob_OrbitPowerData.model.connect('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses','Power.eclipses')
    
    # prob_Link.set_solver_print(level=0) # print msg only if the solver fails
    # prob_Link.model.approx_totals() # not necessary
    
    # case recorder
    # Create a recorder
    recorder = om.SqliteRecorder("reports/casesOrbitPowerData.sql")

    # Attach recorder to the problem
    prob_OrbitPowerData.add_recorder(recorder)

    # Attach recorder to the driver
    prob_OrbitPowerData.driver.add_recorder(recorder)
    # seting up the problem
    prob_OrbitPowerData.setup()

    #check initialization
    print("After initialization")
    print(f"a = {prob_OrbitPowerData.get_val('OrbitalDynamics.a')}")
    
    prob_OrbitPowerData.run_model()
    # check after single solver run
    print("*******After initial solver run:*******")
    prob_OrbitPowerData.list_problem_vars(driver_scaling=False, cons_opts=['min', 'max'])
    #display time evolutions
    eclipses = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkRate')
    charge = prob_OrbitPowerData.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")

    # execute optimization
    prob_OrbitPowerData.run_driver()
    # get results
    print("*******After optimization run:*******")
    prob_OrbitPowerData.list_problem_vars(driver_scaling=False, cons_opts=['min','max'])
    #display time evolutions
    eclipses = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.eclipses')
    gsVisibility = prob_OrbitPowerData.get_val('OrbitalDynamics.OrbitalDynamicsVisibility.gsVisibility')
    downlinkCumulative = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkCumulative')
    downlinkRate = prob_OrbitPowerData.get_val('Data.DataDownload.downlinkRate')
    charge = prob_OrbitPowerData.get_val('Power.charge')
    plotEvolutions(eclipses, gsVisibility, downlinkCumulative, downlinkRate, charge, "./data/input.yaml")
    
    plotConvergence("reports/casesOrbitPowerData.sql")
    # generate N2
    om.n2(prob_OrbitPowerData, "N2_diagrams/separate_disciplines/N2-OrbitPowerDataDisciplines.html")
# ---------------------------------runs----------------------------------------------------------------------

if __name__== "__main__":
    # problemLinkImplementation()
    # problemDataImplementation()
    # problemOrbitAndLink()
    # problemOrbitAndData()
    # problemOrbitPowerData()
    problemOrbitPowerDataRecording()
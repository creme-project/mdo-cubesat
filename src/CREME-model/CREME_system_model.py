# -*- coding: utf-8 -*-
"""
MDA implementation of the first (simple) CREME model. Generates N2 matrix along with XDSM diagram
"""
import openmdao.api as om
from CREME_MDA import *
# XDSM generation
from omxdsm import write_xdsm

prob_CREME = om.Problem()
prob_CREME.model = CREME_MDA()
# seting up the problem
prob_CREME.model.add_design_var('OrbitDynamics.a')
prob_CREME.model.add_design_var('OrbitDynamics.exc')
prob_CREME.model.add_design_var('OrbitDynamics.inc')
prob_CREME.model.add_design_var('OrbitDynamics.raan')
prob_CREME.model.add_design_var('OrbitDynamics.aop')
#prob_CREME.model.add_design_var('OrbitDynamics.ta')
#constraints
prob_CREME.model.add_constraint('Power.charge', lower=0.7) #30% discharge
prob_CREME.model.add_constraint('Link.marginUpload', lower=9.6) #dB
prob_CREME.model.add_constraint('Link.marginDownload', lower=10.5) #dB
# objectives
prob_CREME.model.add_objective('downlinkTotal')
prob_CREME.setup()

# left default values and run the model. Otherwise use Prob_CREME.set_val('varName')
prob_CREME.run_model()
# Check link
marginDownload = prob_CREME.get_val('Link.marginDownload')
print(f"Margin Downlink, dB:{marginDownload}")
marginUpload = prob_CREME.get_val('Link.marginUpload')
print(f"Margin Uplink, dB:{marginUpload}")
# Check Power
charge = prob_CREME.get_val('Power.charge')
mode = prob_CREME.get_val('Power.mode')
print("Power discipline")
print(f"Battery charge state, Wh:{charge}")
print(f"Operation mode: {mode}")
# Check Data
downlinkTotal = prob_CREME.get_val('Data.downlinkTotal')
downlinkRate = prob_CREME.get_val('Data.DataDownload.downlinkRate')
downlinkCumulative = prob_CREME.get_val('Data.DataDownload.downlinkCumulative')
print("Data discipline")
print(f"Total Downloaded, Mbit:\t{downlinkTotal}")
print(f"Download rate, bit/s:\t{downlinkRate}")
print(f"Download over time, Mbit:{downlinkCumulative}")

# generate N2
om.n2(prob_CREME, 'N2_diagrams/N2-CREME_MDA.html')
# generate XDSM
# Write output. PDF will only be created, if pdflatex is installed
# to be resolved - output of designVars* from each element of the group - that shouldn't be happening
write_xdsm(prob_CREME, filename='XDSM_diagrams/CREME_XDSM_pyxdsm', out_format='html', show_browser=True,
            quiet=True, output_side='left', include_indepvarcomps=True)

# --------------plotting---------------
time = np.arange(0, len(prob_CREME.model.get_val('OrbitDynamics.eclipses')), 1)
fig, axs = plt.subplots(3)
axs[0].plot(time, prob_CREME.get_val('OrbitDynamics.eclipses'), '#8888FF')
axs[0].plot(time, prob_CREME.get_val('OrbitDynamics.gsVisibility'), 'b')
axs[0].set_title("Eclipses and Visibility")
axs[1].plot(time, prob_CREME.get_val('Data.DataDownload.downlinkCumulative'), 'k')
ax2=axs[1].twinx()    
ax2.plot(time, prob_CREME.get_val('Data.DataDownload.downlinkRate'), 'b')
axs[1].set_title("Download")
axs[2].plot(time, prob_CREME.get_val('Power.charge'))    

plt.show()
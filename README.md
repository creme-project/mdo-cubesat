# Nanosatellite Preliminary Design Optimisation #
## Description ##
Implementation of preliminary design Multi Disciplinary Optimization (MDO) Of a CubeSat using [OpenMDAO](https://openmdao.org/newdocs/versions/latest/main.html) and [GEMSEO](https://gemseo.readthedocs.io/en/stable/) (Not yet included) frameworks.

![N2 diagram example](./figures/N2.png)

### Directories ##
- **doc** - Bibliographic research archive. Contains all the materials used in this project.
- **src** - Files with project code. Contains tutorials with examples from [OpenMDAO website](https://openmdao.org/newdocs/versions/latest/main.html), a placeholder model of the simulated system to verify top-level MDA and MDO structures.
    - _CREME-model_ : Implementation of MDA and MDO of a simplified CubeSat model based on previous work on the CREME project.
    - _CREME-model\_advanced_ : Implementation of a MDAO of a more advanced CubeSat model based on previous work on the CREME project.
    - _CREME-model_\_advanced_mass : Implementation of a MDAO of a more advanced CubeSat modelincluding a mass.
    - _CREME-model\_placeholder_ : Placeholder model of the CREME CubeSat to visualize disciplines' connections using OpenMDAO API
    - _OpenMDAO\_tutorials_ : Tutorials of the basic concepts from the OpenMDAO library.

## Quickstart guide ##
- follow the installation procedure described in part **Installation**
- Modify following paths in `input.yaml`:
    - `GMAT_BIN_linux` (e.g. /home/dcas/pa.lamos/Programs/GMAT/R2020a/bin/GmatConsole-R2020a # Attention! Be sure to use the CONSOLE version) 
    - ̀  `main_dir` (e.g. /home/dcas/pa.lamos/Documents/Stage DCAS/2022-pir-patrik_lamos/src/CREME-model/ # Attention with a space in name, GMAT wants it without '\\') 
- Make sure you are using the right envitonment. To activate a specific conda environment use :
`conda activate my_env`
- __Simple model__: in `CREME-model/`
    - generate N2 diagram and XDSM diagram of the simple CREME model using `conda  CREME_system_model.py`
    - run COBYLA algorithm optimization using `conda  CREME_MDAO.py`. This command shows a time evolution of variables for the initial design, performs the optimization procedure, shows a time evolution of variables for the final design, prints all variable values in the console and plots the convergencve diagram of variabels.
    - run a Differential evolution algorithm optimization using `conda  CREME_MDAO_GA.py`. All same as for the COBYLA.
- __Advanced model__: in `CREME-model_advanced/`
    - run COBYLA algorithm optimization using `conda  CREME_Optimization_advanced.py`. This command shows a time evolution of variables for the initial design, performs the optimization procedure, shows a time evolution of variables for the final design, prints all variable values in the console and plots the convergencve diagram of variabels.
    - to run a Differential evolution algorithm optimization uncomment the om.DifferentialEvolutionDriver() and run `conda  CREME_Optimization_advanced.py`. All same as for the COBYLA.
- __Advanced model mass__: in `CREME-model_advanced_mass/`
    - run COBYLA algorithm optimization using `conda  CREME_Optimization_advanced.py`. This command shows a time evolution of variables for the initial design, performs the optimization procedure, shows a time evolution of variables for the final design, prints all variable values in the console and plots the convergencve diagram of variabels.
    - to run a Differential evolution algorithm optimization uncomment the om.DifferentialEvolutionDriver() and run `conda  CREME_Optimization_advanced.py`. All same as for the COBYLA.

## Installation ##
- download and install [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) :  
`bash Anaconda-latest-Linux-x86_64.sh`
- Setup [conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) :  
`conda create -n my_env -c anaconda python=3.9.7`  
`conda activate my_env`  
`conda install pip`
- Install teh packages listed in **Dependencies**

## Dependencies ##
- _List of sofrware here_
- OpenMDAO 3.19 `pip install OpenMDAO`
- numpy 1.22.3 `conda install numpy`
- yaml 0.2.5 `conda install -c anaconda yaml`
- yamlloader 1.1.0 `pip install yamlloader`
- matplotlib 3.5.2 `pip install matplotlib`
- omxdsm 0.6 `pip install omxdsm@https://github.com/onodip/OpenMDAO-XDSM/tarball/master` (still a bit buggy)

- GMAT R2020a - [download](https://sourceforge.net/projects/gmat/) the archive and extract it to a folder of your choice.
---

## Issues ##
- N2 generation in OpenMDAO - make sure to have the latest version of OpenMDAO (3.19). With 3.18 there are some issues with displaying the N2 matrix of a system.
- XDSM generation using [omxdsm](https://github.com/onodip/OpenMDAO-XDSM) is not 100% conform to the XDSM formalism. The documentation is not very well done, but it is expected as omxdsm is a work-in-progresss.
- ExternalCodeComp:
    - When defining `self.options['command']` to be executed by this class, use list instead of a simple string. Otherwise OpenMDAO    may encountre problem when parsing the command and checking its validity.
    - Path to files - attention on space char in the path, it may cause issues (i.e. pass the path withouth the leading `\` before a space or avoid using it altogether).
- Attention when accessing variables - When accessing values form `inputs/outputs['...']` (e.g. in `compute(self, inputs, outputs)`) this call returns always an array, even if the input/output is defined as a scalar.
- Passing eclipses and GS visibility data - formally a discrete-values vector (0;1), but in order for optimizer to understand the dependency between visibility at the specific time interval and downloaded data it is treated as a continuous variable in the Data disciplines.
- OpenMDAO automatically generates repports when certain execution commands are called. More details can be found in [Reports System](https://openmdao.org/newdocs/versions/latest/features/reports/reports_system.html?highlight=report)
